L-system generator
==================
L-system (Lindenmayer system) generator.

See https://en.wikipedia.org/wiki/L-system

Parallelization with `PyOpenCL`_
as a project
for the `Multicore Programming`_ course from VUB.

.. _`Multicore Programming`: http://soft.vub.ac.be/teaching/multicore/
.. _`PyOpenCL`: https://mathema.tician.de/software/pyopencl/



Table of contents
=================

.. toctree::
   :maxdepth: 2

   simple_l_system
   fast_l_system
   array_l_system
   opencl_l_system
   opt_opencl_l_system

   turtle
   opencl_turtle
   opt_opencl_turtle

   prog_l_system

   benchmarks
   benchmarks_turtle
   opencl_find_limits
   opencl_infos



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2018 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _GPLv3: http://www.gnu.org/licenses/gpl.html

.. |GPLv3| image:: _static/img/gplv3-88x31.png



Author: 🌳  Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
==================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: _static/img/OPi--41x34--t.png
