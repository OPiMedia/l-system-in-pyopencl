# -*- coding: latin-1 -*-

"""
Reimplementation of simple_l_system.LSystem and fast_l_system.FastLSystem
with OpenCL instead work on NumPy arrays.

Alphabet must be contains at most 255 symbols.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function

import os.path
import sys

import numpy as np
import pyopencl as cl

import l_system.simple_l_system as simple_l_system
import l_system.array_l_system as array_l_system

KERNEL_PATH = os.path.dirname(__file__) + '/kernel/'


def device_get_max_compute_units(device):
    """
    Return the max compute units of device.

    :param device: OpenCL device

    :return: int
    """
    return int(device.max_compute_units)


def device_get_max_work_group_size(device):
    """
    Return the max work group size of device.

    :param device: OpenCL device

    :return: int
    """
    return int(device.max_work_group_size)


def device_get_max_work_item_sizes(device):
    """
    Return the max work item sizes of device.

    :param device: OpenCL device

    :return: (int, int, int)
    """
    return tuple(int(size) for size in device.max_work_item_sizes)


def device_is_ok(device, nb_work_group, nb_work_item):
    """
    Try to return an evalation of the possibility
    to have nb_work_group work groups of nb_work_item work items.

    If device is NVIDIA
    then return True iff there has enough work groups and work items,
    else consider that is a CPU and return True iff only has enough compute units.

    :param device: OpenCL device
    :param nb_work_group: int > 0
    :param nb_work_item: int > 0

    :return: bool
    """
    assert isinstance(nb_work_group, int), type(nb_work_group)
    assert nb_work_group > 0, nb_work_group

    assert isinstance(nb_work_item, int), type(nb_work_item)
    assert nb_work_item > 0, nb_work_item

    return (((nb_work_item <= device_get_max_work_item_sizes(device)[0]) and
             (nb_work_group * nb_work_item <= device_get_max_work_group_size(device)))
            if device_is_nvidia(device)
            else nb_work_group * nb_work_item <= device_get_max_compute_units(device))


def device_is_nvidia(device):
    """
    If the vendor of device contains NVIDIA
    then return True,
    else return False.

    :param device: OpenCL device

    :return: bool
    """
    return 'NVIDIA' in device.vendor.upper()


def get_device(platform_id, device_id):
    """
    Return the given device of the given platform OpenCL.

    :param platform_id: int >= 0
    :param device_id: int >= 0

    :return: None or OpenCL device
    """
    assert isinstance(platform_id, int), type(platform_id)
    assert platform_id >= 0, platform_id

    assert isinstance(device_id, int), type(device_id)
    assert device_id >= 0, device_id

    platforms = cl.get_platforms()
    if platform_id >= len(platforms):
        return None

    devices = platforms[platform_id].get_devices()
    if device_id >= len(devices):
        return None

    return devices[device_id]


class OpenCLLSystem(array_l_system.ArrayLSystem):
    """
    Reimplementation of array_l_system.ArrayLSystem
    with main expansion in OpenCL.
    """

    def __init__(self, alphabet, axiom, production, name='',
                 kernel_filename=None,
                 device=None, nb_work_item=4,
                 debug=None):
        """
        Initialize the L-system (alphabet, axiom, production rules).

        Alphabet is maybe reordered to beginning by symbols of production rules.

        Production rule can't be constant.
        And at least one rule must be contains at least two symbols.

        :param alphabet: str of unique symbols
        :param axiom: str of initial symbols
        :param production: dict symbol: (str of symbols)
        :param name: str
        :param kernel_filename: None or str
        :param device: None or OpenCL device
        :param nb_work_item: number of rules <= int <= maximum number of work items supported by device
        :param debug: None or bool
        """
        assert isinstance(nb_work_item, int), type(nb_work_item)
        assert nb_work_item >= 1, nb_work_item
        assert nb_work_item >= len(production), (nb_work_item, len(production))

        assert (kernel_filename is None) or isinstance(kernel_filename, str), type(kernel_filename)

        assert (debug is None) or isinstance(debug, bool), type(debug)

        array_l_system.ArrayLSystem.__init__(self, alphabet, axiom, production, name)

        # OpenCL params and kernel
        self._cl_duration_ns = 0
        self._cl_nb_work_item = nb_work_item

        self._cl_context = (cl.create_some_context() if device is None
                            else cl.Context((device, )))

        options = ['-I', KERNEL_PATH,
                   '-D', 'DEFAULT={}'.format(array_l_system.DEFAULT),
                   '-D', 'G_SIZE={}'.format(self._cl_nb_work_item),
                   '-D', 'NB_RULE={}'.format(len(production))]

        self._cl_kernel = open(KERNEL_PATH + ('l_system.cl' if kernel_filename is None
                                              else kernel_filename)).read()

        self._cl_ndebug = (not __debug__ if debug is None
                           else not debug)

        if self._cl_ndebug:
            options.extend(('-D', 'NDEBUG'))
        else:
            print('OpenCL in DEBUG mode!', file=sys.stderr)

        self._cl_program = cl.Program(self._cl_context, self._cl_kernel).build(options=options)

    def cl_duration_ns(self):
        """
        Return the duration used by the OpenCL kernel, in nanoseconds ns = 10^{-9} s.

        :return: int
        """
        return self._cl_duration_ns

    def cl_duration_s(self):
        """
        Return the duration used by the OpenCL kernel, in seconds.

        :return: float
        """
        return float(self._cl_duration_ns) / (10**9)

    def production_iterate(self, nb_iteration, cl_kernel_fct=None):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        If nb_iteration == 0
        then return an empty dict.

        If cl_kernel_fct is None
        then run the kernel function self._cl_program.expand,
        else run the kernel function cl_kernel_fct.

        Computed in nb_iteration steps.

        :param nb_iteration: int >= 0
        :param cl_kernel_fct: None or kernel function from self._cl_program

        :return: dict symbol: (str of symbols)
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration == 0:
            return dict()
        elif nb_iteration == 1:
            return dict(self._production)
        else:                    # Expand all production rules to be equivalent to nb_iteration
            nb_rule = len(self._production)

            r_max_nb_variable = simple_l_system.rule_max_nb_variable(self._production)
            r_max_nb_constant = simple_l_system.rule_max_nb_constant(self._alphabet, self._production)
            assert self._r_max_size <= r_max_nb_variable + r_max_nb_constant

            variable, constant = simple_l_system.split_string(self._current, self._alphabet, self._production)

            s_nb_variable = len(variable)
            s_nb_constant = len(constant)
            s_size = len(self._current)

            assert s_nb_variable + s_nb_constant == s_size

            r_array_size \
                = simple_l_system.rule_iter_upper_bounds(nb_iteration - 1,
                                                         r_max_nb_variable, r_max_nb_constant, self._r_max_size)[2] * self._r_max_size

            # !!! FIXME !!!
            # There is probably something wrong in the evaluation of r_array_size.
            # The theoretical and the practical computation seem correct,
            # but the expected size must be multiply by 2 to avoid not understood error
            # with some L-system and/or some nb of iterations
            #      and/or nb of work units and/or GPU or CPU
            # !!!
            r_array_size *= 2

            a_mapping = array_l_system.alphabet_mapping(self._alphabet)

            # Array for result
            h_result = np.empty(2).astype(np.uint32)  # receive {r_expanded_max_size, is_swapped}
            if __debug__:
                h_result.fill(666)  # fake value to be sure to receive a result from the kernel

            # Array for production rules of the L-system, init with self._production
            production_array = array_l_system.empty_production_array(self._r_max_size * nb_rule)
            array_l_system.copy_production_to_production_array(self._production, a_mapping,
                                                               self._r_max_size,
                                                               production_array)

            # Array for production rules to be expand, init with self._production
            production_array_to_expand = array_l_system.default_production_array(r_array_size * nb_rule)
            for i_rule in range(nb_rule):
                production_array_to_expand[r_array_size * i_rule:
                                           r_array_size * i_rule + self._r_max_size] \
                    = production_array[self._r_max_size * i_rule:
                                       self._r_max_size * (i_rule + 1)]

            # Array for production rules expanded result
            production_expanded_array = array_l_system.empty_production_array(r_array_size * nb_rule)

            # OpenCL context and queue
            queue = cl.CommandQueue(self._cl_context,
                                    properties=cl.command_queue_properties.PROFILING_ENABLE)

            # OpenCL buffers
            mf = cl.mem_flags
            d_production = cl.Buffer(self._cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR,
                                     hostbuf=production_array)
            d_production_to_expand = cl.Buffer(self._cl_context, mf.READ_WRITE | mf.COPY_HOST_PTR,
                                               hostbuf=production_array_to_expand)
            d_production_expanded = cl.Buffer(self._cl_context, mf.READ_WRITE | mf.COPY_HOST_PTR,
                                              hostbuf=production_expanded_array)

            d_result = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                                 hostbuf=h_result)

            # Run OpenCL kernel
            f = (self._cl_program.expand if cl_kernel_fct is None
                 else cl_kernel_fct)
            if self._cl_ndebug:  # normal execution
                f.set_scalar_arg_dtypes((np.uint32,
                                         None, np.uint32,
                                         np.uint32,
                                         None, None,
                                         None))
                event = f(queue, (self._cl_nb_work_item, ), (self._cl_nb_work_item, ),
                          nb_iteration,
                          d_production, self._r_max_size,
                          r_array_size,
                          d_production_to_expand, d_production_expanded,
                          d_result)
            else:                # execution with param for OpenCL assertions result
                h_assert = np.empty(2).astype(np.uint32)
                h_assert.fill(-1)
                d_assert = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                                     hostbuf=h_assert)

                f.set_scalar_arg_dtypes((np.uint32,
                                         None, np.uint32,
                                         np.uint32,
                                         None, None,
                                         None,
                                         None))
                event = f(queue, (self._cl_nb_work_item, ), (self._cl_nb_work_item, ),
                          nb_iteration,
                          d_production, self._r_max_size,
                          r_array_size,
                          d_production_to_expand, d_production_expanded,
                          d_result,
                          d_assert)

            queue.finish()

            self._cl_duration_ns += event.profile.end - event.profile.start

            del production_array_to_expand

            del d_production

            # Get results (from d_production_expanded or from d_production_to_expand)
            cl.enqueue_copy(queue, h_result, d_result)
            r_expanded_max_size = int(h_result[0])
            is_swapped = bool(h_result[1])

            cl.enqueue_copy(queue, production_expanded_array,
                            (d_production_to_expand if is_swapped
                             else d_production_expanded))

            del d_production_to_expand
            del d_production_expanded

            if not self._cl_ndebug:  # some assertion failed in OpenCL
                cl.enqueue_copy(queue, h_assert, d_assert)
                if h_assert[0] != 0:
                    print('Assertion failed in the OpenCL kernel! Maybe line: {} with value: {}'
                          .format(*h_assert))

                    exit(1)
                elif h_assert[1] != 0:
                    print('INFO value: {}'.format(h_assert[1]))

            # Build dict of production rules equivalent to nb_iteration
            return array_l_system.production_array_to_production(production_expanded_array,
                                                                 self._alphabet, nb_rule,
                                                                 r_array_size,
                                                                 r_expanded_max_size)


class FastOpenCLLSystem(OpenCLLSystem):
    """
    Reimplementation of OpenCLLSystem
    with the same fast algorithm than fast_l_system.FastLSystem.
    """
    def production_iterate(self, nb_iteration):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        Use the kernel function self._cl_program.fast_expand
        instead the kernel function self._cl_program.expand.

        If nb_iteration == 0
        then return an empty dict.

        Computed in nb_iteration steps.

        :param nb_iteration: int >= 0

        :return: dict symbol: (str of symbols)
        """
        return OpenCLLSystem.production_iterate(self, nb_iteration, self._cl_program.fast_expand)
