# -*- coding: latin-1 -*-

"""
Simple Python implementation of L-system (Lindenmayer system) generator.

See https://en.wikipedia.org/wiki/L-system

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2018
"""

from __future__ import division
from __future__ import print_function

import math


def expanded_size(nb_variable, size,
                  r_max_size):
    """
    For a string of size size and nb_variable variables
    and r_max_size the biggest size of a rule from production rules,
    return the size of the string expanded.

    :param nb_variable: int > 0
    :param size: int > 0
    :param r_max_size: int > 0

    :return: int > 0
    """
    assert isinstance(nb_variable, int), type(nb_variable)
    assert nb_variable > 0, nb_variable

    assert isinstance(size, int), type(size)
    assert size > 0, size

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size > 0, r_max_size

    return size + nb_variable * (r_max_size - 1)


def intceil(value):
    """
    Return value rounded to the smallest integer greater than or equal to value.

    :param value: float

    :return: int
    """
    return int(math.ceil(value))


def intround(value):
    """
    Return value rounded to the closest int.

    :param value: float

    :return: int
    """
    return int(round(value))


def is_production(item):
    """
    Return True if item is a dict of production rule (str: not empty str != to the key),
    else return False.

    :param item: object

    :return: bool
    """
    if not isinstance(item, dict) or not item:
        return False

    for symbol, rule in item.items():
        if not is_symbol(symbol) or not isinstance(rule, str) or (rule == '') or (rule == symbol):
            return False

    return True


def is_symbol(item):
    """
    Return True if item is a symbol (a str of length 1),
    else return False.

    :param item: object

    :return: bool
    """
    return isinstance(item, str) and (len(item) == 1)


def load_l_system(filename):
    """
    Load from a file the definition of a L-system
    and return (name, alphabet, axiom, production rules).

    The format of the file must be:

| ``name``
| ``alphabet``
| ``axiom``
| ``symbol: string of symbol``
| ``symbol: string of symbol``
| ``...``

    :param filename: str, filename to a readable valid file

    :return: (str, str of symbols, symbol, dict symbol: (str of symbols))
    """
    assert isinstance(filename, str), type(filename)
    assert filename != ''

    with open(filename) as filein:
        name = filein.readline().strip()
        alphabet = filein.readline().strip()
        axiom = filein.readline().strip()

        production = dict()
        for line in filein:
            pieces = line.strip().split(':')

            assert len(pieces) == 2, pieces

            symbol = pieces[0].strip()
            production[symbol] = pieces[1].strip()

    return (name, alphabet, axiom, production)


def rule_iter_upper_bounds(nb_iteration,
                           r_max_nb_variable, r_max_nb_constant, r_max_size):
    """
    Return (upper bound of the number of variables of the nb_iteration of a rule,
            upper bound of the number of constants of the nb_iteration of a rule,
            upper bound of the size of the nb_iteration of a rule).

    If nb_iteration == 0
    then return (1, 0, 1).

    :param nb_iteration: int >= 0
    :param r_max_nb_variable: int > 0
    :param r_max_nb_constant: int >= 0
    :param r_max_size: int > 0

    :return: (int >= 0, int >= 0, int >= 0)
    """
    assert isinstance(nb_iteration, int), type(nb_iteration)
    assert nb_iteration >= 0, nb_iteration

    assert isinstance(r_max_nb_variable, int), type(r_max_nb_variable)
    assert r_max_nb_variable > 0, r_max_nb_variable

    assert isinstance(r_max_nb_constant, int), type(r_max_nb_constant)
    assert r_max_nb_constant >= 0, r_max_nb_constant

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size > 0, r_max_size

    if r_max_nb_variable == 1:
        return (1,
                r_max_nb_constant * nb_iteration,
                (r_max_size - 1) * nb_iteration + 1)

    r_bound_variable = r_max_nb_variable**nb_iteration

    frac = (r_bound_variable - 1) // (r_max_nb_variable - 1)

    r_bound_constant = frac * r_max_nb_constant
    r_bound_size = frac * (r_max_size - 1) + 1

    return (r_bound_variable, r_bound_constant, r_bound_size)


def rule_iter_upper_nb_constant(nb_iteration, r_max_nb_variable, r_max_nb_constant):
    """
    Return an upper bound of the number of constants of the nb_iteration of a rule.

    If nb_iteration == 0
    then return 0.

    :param nb_iteration: int >= 0
    :param r_max_nb_variable: int > 0
    :param r_max_nb_constant: int >= 0

    :return: int >= 0
    """
    assert isinstance(nb_iteration, int), type(nb_iteration)
    assert nb_iteration >= 0, nb_iteration

    assert isinstance(r_max_nb_variable, int), type(r_max_nb_variable)
    assert r_max_nb_variable > 0, r_max_nb_variable

    assert isinstance(r_max_nb_constant, int), type(r_max_nb_constant)
    assert r_max_nb_constant >= 0, r_max_nb_constant

    if r_max_nb_variable == 1:
        return r_max_nb_constant * nb_iteration

    r_bound_variable = r_max_nb_variable**nb_iteration

    return ((r_bound_variable - 1) // (r_max_nb_variable - 1) *
            r_max_nb_constant)


def rule_iter_upper_nb_variable(nb_iteration, r_max_nb_variable):
    """
    Return an upper bound of the number of variables of the nb_iteration of a rule.

    :param nb_iteration: int >= 0
    :param r_max_nb_variable: int > 0

    If nb_iteration == 0
    then return 1.

    :return: int >= 0
    """
    assert isinstance(nb_iteration, int), type(nb_iteration)
    assert nb_iteration >= 0, nb_iteration

    assert isinstance(r_max_nb_variable, int), type(r_max_nb_variable)
    assert r_max_nb_variable > 0, r_max_nb_variable

    return r_max_nb_variable**nb_iteration


def rule_iter_upper_size(nb_iteration, r_max_nb_variable, r_max_size):
    """
    Return an upper bound of the size of the nb_iteration of a rule.

    If nb_iteration == 0
    then return 1.

    :param nb_iteration: int >= 0
    :param r_max_nb_variable: int > 0
    :param r_max_size: int > 0

    :return: int >= 0
    """
    assert isinstance(nb_iteration, int), type(nb_iteration)
    assert nb_iteration >= 0, nb_iteration

    assert isinstance(r_max_nb_variable, int), type(r_max_nb_variable)
    assert r_max_nb_variable > 0, r_max_nb_variable

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size > 0, r_max_size

    if r_max_nb_variable == 1:
        return (r_max_size - 1) * nb_iteration + 1

    r_bound_variable = r_max_nb_variable**nb_iteration

    return ((r_bound_variable - 1) // (r_max_nb_variable - 1) *
            (r_max_size - 1) + 1)


def rule_max_nb_constant(alphabet, production):
    """
    Return the biggest number of constants of a rule from production.

    Production rule can't be constant.

    :param alphabet: str of unique symbols
    :param production: dict symbol: (str of symbols)

    :return: int >= 0
    """
    assert isinstance(alphabet, str), type(alphabet)
    assert len(set(alphabet)) == len(alphabet), alphabet

    assert isinstance(production, dict), type(production)

    return (max(len([None for symbol in rule
                     if symbol not in production])
                for rule in production.values())
            if production
            else len(alphabet))


def rule_max_nb_variable(production):
    """
    Return the biggest number of variables of a rule from production.

    Production rule can't be constant.

    :param production: dict symbol: (str of symbols)

    :return: int >= 0
    """
    assert isinstance(production, dict), type(production)

    return (max(len([None for symbol in rule
                     if symbol in production])
                for rule in production.values())
            if production
            else 0)


def rule_max_size(production):
    """
    Return the biggest size of a rule from production.

    :param production: dict symbol: (str of symbols)

    :return: int >= 0
    """
    assert isinstance(production, dict), type(production)

    return (max(len(rule) for rule in production.values())
            if production
            else 0)


def split_alphabet(alphabet, production):
    """
    Return the alphabet splitted in (variables, constants).

    Production rule can't be constant.

    :param alphabet: str of unique symbols
    :param production: dict symbol: (str of symbols)

    :return: (str of symbols, str of symbols)
    """
    assert isinstance(alphabet, str), type(alphabet)
    assert len(set(alphabet)) == len(alphabet), alphabet

    assert isinstance(production, dict), type(production)
    if __debug__:
        for symbol, rule in production.items():
            assert symbol != rule, symbol

    constants = ''
    variables = ''

    for symbol in alphabet:
        if symbol in production:
            variables += symbol
        else:
            constants += symbol

    return (variables, constants)


def split_string(string, alphabet, production):
    """
    Return the alphabet splitted in (variables, constants).

    Production rule can't be constant.

    :param string: str of symbols
    :param alphabet: str of unique symbols
    :param production: dict symbol: (str of symbols)

    :return: (str of symbols, str of symbols)
    """
    assert isinstance(string, str), type(string)

    assert isinstance(alphabet, str), type(alphabet)
    assert len(set(alphabet)) == len(alphabet), alphabet

    assert isinstance(production, dict), type(production)
    if __debug__:
        for symbol, rule in production.items():
            assert symbol != rule, symbol

    constants = ''
    variables = ''

    for symbol in string:
        if symbol in production:
            variables += symbol
        else:
            constants += symbol

    return (variables, constants)


def string_iter_upper_bounds(nb_iteration,
                             r_max_nb_variable, r_max_nb_constant, r_max_size,
                             s_nb_variable, s_nb_constant, s_size):
    """
    Return (upper bound of the number of variables of nb_iteration expansions of a string,
            upper bound of the number of constants of nb_iteration expansions of a string,
            upper bound of the size of nb_iteration expansions of a string).

    The string has s_nb_variable variables and a size of s_size.

    If nb_iteration == 0
    then return (1, 0, 1).

    :param nb_iteration: int >= 0
    :param r_max_nb_variable: int > 0
    :param r_max_nb_constant: int >= 0
    :param r_max_size: int >= r_max_nb_variable
    :param s_nb_variable: int > 0
    :param s_nb_constant: int >= 0
    :param s_size: int > 0

    :return: (int >= 0, int >= 0, int >= 0)
    """
    assert isinstance(nb_iteration, int), type(nb_iteration)
    assert nb_iteration >= 0, nb_iteration

    assert isinstance(r_max_nb_variable, int), type(r_max_nb_variable)
    assert r_max_nb_variable > 0, r_max_nb_variable

    assert isinstance(r_max_nb_constant, int), type(r_max_nb_constant)
    assert r_max_nb_constant >= 0, r_max_nb_constant

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size >= r_max_nb_variable, (r_max_size, r_max_nb_variable)

    assert isinstance(s_nb_variable, int), type(s_nb_variable)
    assert s_nb_variable > 0, s_nb_variable

    assert isinstance(s_nb_constant, int), type(s_nb_constant)
    assert s_nb_constant >= 0, s_nb_constant

    assert isinstance(s_size, int), type(s_size)
    assert s_size > 0, s_size

    if r_max_nb_variable == 1:
        return (s_nb_variable,
                s_nb_variable * r_max_nb_constant * nb_iteration + s_nb_constant,
                (r_max_size - 1) * s_nb_variable * nb_iteration + s_size)

    power = r_max_nb_variable**nb_iteration
    frac = (power - 1) // (r_max_nb_variable - 1)

    s_bound_variable = power * s_nb_variable
    s_bound_constant = frac * r_max_nb_constant * s_nb_variable + s_nb_constant
    s_bound_size = frac * (r_max_size - 1) * s_nb_variable + s_size

    return (s_bound_variable, s_bound_constant, s_bound_size)


class LSystem:
    """
    Class of a mutable L-system (alphabet, axiom, production rules)
    with a current state initially set to axiom
    and method to expand it.

    See https://en.wikipedia.org/wiki/L-system

    See "The Algorithmic Beauty of Plants"
    http://algorithmicbotany.org/papers/abop/abop.pdf

    :example:
      ``LSystem('XYF+-', 'FX', {'X': 'X+YF+', 'Y': '-FX-Y'}, 'dragon curve')``
      for the dragon curve
      https://en.wikipedia.org/wiki/L-system#Example_6:_Dragon_curve
    """

    def __init__(self, alphabet, axiom, production, name=''):
        """
        Initialize the L-system (alphabet, axiom, production rules).

        Production rule can't be constant.

        :param alphabet: str of unique symbols
        :param axiom: str of initial symbols
        :param production: dict symbol: (str of symbols)
        :param name: str
        """
        assert isinstance(alphabet, str), type(alphabet)
        assert len(set(alphabet)) == len(alphabet), alphabet

        assert isinstance(axiom, str), type(axiom)
        assert isinstance(production, dict), type(production)
        assert isinstance(name, str), type(name)
        if __debug__:
            for symbol in axiom:
                assert is_symbol(symbol), symbol

            for symbol in production:
                assert is_symbol(symbol), symbol
                assert symbol in alphabet, symbol
                assert production[symbol] != symbol, symbol

                for result in production[symbol]:
                    assert result in alphabet, result

        self._alphabet = alphabet
        self._axiom = axiom
        self._production = dict(production)
        self._name = name

        self._current = self._axiom
        self._iteration = 0

    def __str__(self):
        """
        Return the current state.

        :return: str of symbols
        """
        return self._current

    def expand(self, nb_iteration=1):
        """
        Expand and modify the current state nb_iteration times.

        :param nb_iteration: int >= 0
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        for _ in range(nb_iteration):
            self._current = self.expand_string(self._current, self._production)
            self._iteration += 1

    def expand_string(self, string, production):
        """
        Return string with each symbol expanded with the production rules.

        :param string: str of symbols
        :param production: dict symbol: (str of symbols)

        :return: str of symbols
        """
        assert isinstance(string, str), type(string)

        return ''.join(self.expand_symbol(symbol, production)
                       for symbol in string)

    def expand_symbol(self, symbol, production):
        """
        Return symbol expanded with the production rules.

        :param symbol: symbol (a str of length 1)
        :param production: dict symbol: (str of symbols)

        :return: symbol
        """
        assert is_symbol(symbol), symbol
        assert symbol in self._alphabet, symbol
        assert isinstance(production, dict), type(production)

        return production.get(symbol, symbol)

    def iteration(self):
        """
        Return the current number of iteration.

        :return: int >= 0
        """
        return self._iteration

    def name(self):
        """
        Return the name of the L-system.

        :return: str
        """
        return self._name

    def production_iterate(self, nb_iteration):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        If nb_iteration == 0
        then return an empty dict.

        Computed in nb_iteration steps.

        :param nb_iteration: int >= 0

        :return: dict symbol: (str of symbols)
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration == 0:
            return dict()
        else:
            production_iter = dict(self._production)

            for _ in range(1, nb_iteration):
                new_production_iter = dict()
                for symbol, string in production_iter.items():
                    new_production_iter[symbol] = self.expand_string(string, self._production)
                production_iter = new_production_iter

            return production_iter
