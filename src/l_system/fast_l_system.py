# -*- coding: latin-1 -*-

"""
Faster version of LSystem.

Expand the alphabet instead to directly expand the axiom.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 13, 2018
"""

from __future__ import division
from __future__ import print_function

import l_system.simple_l_system as simple_l_system


class FastLSystem(simple_l_system.LSystem):
    """
    Faster version of simple_l_system.LSystem.

    Expand the alphabet instead to directly expand the axiom
    and the iterated expansion of the alphabet is computed
    by a similar way to the classical exponentiating by squaring algorithm.
    """

    def expand(self, nb_iteration=1):
        """
        Expand and modify the current state nb_iteration times.

        Use production_iterate() to compute in log_2(nb_iteration) steps,

        :param nb_iteration: int >= 0
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        production = self.production_iterate(nb_iteration)
        self._current = self.expand_string(self._current, production)
        self._iteration += nb_iteration

    def production_iterate(self, nb_iteration):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        If nb_iteration == 0
        then return an empty dict.

        Computed in log_2(nb_iteration) steps,
        in a similar way to the classical exponentiating by squaring algorithm.

        :param nb_iteration: int >= 0

        :return: dict symbol: (str of symbols)
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration == 0:
            return dict()
        else:
            production_iter = dict(self._production)

            # Binary representation without the most significant figure
            bins = bin(nb_iteration)[3:]

            for bit in bins:
                new_production_iter = dict()
                for symbol, string in production_iter.items():
                    # Expand this symbol by apply production_iter to itself
                    string = self.expand_string(string, production_iter)
                    if bit == '1':
                        # Apply one more time the initial production
                        string = self.expand_string(string, self._production)
                    new_production_iter[symbol] = string
                production_iter = new_production_iter

            return production_iter
