# -*- coding: latin-1 -*-

"""
Reimplementation of simple_l_system.LSystem and fast_l_system.FastLSystem
with NumPy arrays instead "dynamic" used of strings.

Alphabet must be contains at most 255 symbols.

The goal of that was to have the algorithms on a form
more easily convertible in OpenCL
and build classes to derive.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 19, 2018
"""

from __future__ import division
from __future__ import print_function

import numpy as np

import l_system.simple_l_system as simple_l_system


DEFAULT = 255  # value to mark absence of symbol


def alphabet_mapping(alphabet):
    """
    Return a dict that associate to each symbol of alphabet
    its index in alphabet.

    :example: alphabet_mapping('YXA') == {'Y': 0, 'X': 1, 'A': 2}

    :param alphabet: str of unique symbols

    :return: dict symbol: (1 <= int <= len(alphabet))
    """
    assert isinstance(alphabet, str), type(alphabet)
    assert len(set(alphabet)) == len(alphabet), alphabet

    a_mapping = dict()
    for i, symbol in enumerate(alphabet):
        assert symbol not in a_mapping, symbol

        a_mapping[symbol] = int(i)

    return a_mapping


def alphabet_mapping_reverse(alphabet, array, default=''):
    """
    Return a string corresponding to the array
    with each number substituted by the symbol of the alphabet.
    Each DEFAULT is substituted by default.

    :example: alphabet_mapping('YXA', numpy.array((0, DEFAULT, 1, DEFAULT)), '_') == 'Y_X_'

    :param alphabet: str of symbols
    :param array: NumPy array
    :param default: str

    :return: str
    """
    assert isinstance(alphabet, str), type(alphabet)
    assert isinstance(array, np.ndarray), type(array)
    assert isinstance(default, str), type(default)

    return ''.join((alphabet[i] if i < DEFAULT
                    else default)
                   for i in array)


def copy_production_to_production_array(production, a_mapping, r_max_size,
                                        production_array):
    """
    Copy each rule of production production_array.

    Each rule occupy r_max_size items (with 0 values to complete length).

    Order of rules are given by a_mapping.

    :param production: dict symbol: (str of symbols)
    :param a_mapping: (0 <= int < len(alphabet))
    :param r_max_size: int > 0 the biggest size of a rule from production
    :param production_array: NumPy array of length at least len(production) * r_max_size
    """
    assert isinstance(production, dict), type(production)
    assert isinstance(a_mapping, dict), type(a_mapping)

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size > 0, r_max_size

    assert isinstance(production_array, np.ndarray), type(production_array)
    assert production_array.shape[0] >= len(production) * r_max_size

    for symbol, rule in production.items():
        assert symbol in a_mapping, (symbol, a_mapping)

        offset = a_mapping[symbol] * r_max_size
        for i in range(r_max_size):
            production_array[offset + i] = a_mapping.get(rule[i:i + 1], DEFAULT)


def default_production_array(size):
    """
    Return a NumPy array of size uint8 initialized with value DEFAULT.

    :param size: int >= 0

    :return: NumPy array
    """
    assert isinstance(size, int), type(size)
    assert size >= 0, size

    return np.full(size, DEFAULT).astype(np.uint8)


def empty_production_array(size):
    """
    Return a (uninitialized) NumPy array of size uint8.

    :param size: int >= 0

    :return: NumPy array
    """
    assert isinstance(size, int), type(size)
    assert size >= 0, size

    return np.empty(size).astype(np.uint8)


def production_array_expand(nb_rule, production_array, r_max_size,
                            production_array_to_expand, r_max_size_to_expand,
                            production_expanded_array, r_expanded_max_size):
    """
    Expand production_array_to_expand production rules
    with production_array production rules
    and store the result in production_expanded_array.

    production_array_to_expand and production_array
    don't contains DEFAULT values in middle of a rule.

    The results production_expanded_array is "compacted"
    such that it don't contains DEFAULT values in middle of a rule,
    and production_array_expand() return the max size corresponding to this result.

    :param nb_rule: int > 0
    :param production_array: NumPy array of length at least len(production) * r_max_size
    :param r_max_size: int > 0
    :param production_array_to_expand: NumPy array of length at least len(production) * r_max_size
    :param r_max_size_to_expand: int > 0
    :param production_expanded_array: NumPy array of length at least len(production) * r_expanded_max_size
    :param r_expanded_max_size: r_max_size_to_expand < int <= r_max_size

    :return: int > 0
    """
    assert isinstance(nb_rule, int), type(nb_rule)
    assert nb_rule > 0, nb_rule

    assert isinstance(production_array, np.ndarray), type(production_array)
    assert production_array.shape[0] >= nb_rule * r_max_size

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size > 0, r_max_size

    assert isinstance(production_array_to_expand, np.ndarray), type(production_array_to_expand)
    assert production_array_to_expand.shape[0] >= nb_rule * r_max_size_to_expand

    assert isinstance(r_max_size_to_expand, int), type(r_max_size_to_expand)
    assert r_max_size_to_expand > 0, r_max_size_to_expand

    assert isinstance(production_expanded_array, np.ndarray), type(production_expanded_array)
    assert production_expanded_array.shape[0] >= nb_rule * r_expanded_max_size

    assert isinstance(r_expanded_max_size, int), type(r_max_size_to_expand)
    assert r_max_size_to_expand < r_expanded_max_size <= r_max_size_to_expand * r_max_size, \
        (r_max_size_to_expand, r_max_size, r_expanded_max_size)

    r_expanded_max_size = r_max_size_to_expand * r_max_size
    new_r_expanded_max_size = 0

    for i_rule_to_expand in range(nb_rule):
        offset_to_expand = i_rule_to_expand * r_max_size_to_expand
        offset_expanded_part = i_rule_to_expand * r_expanded_max_size
        nb_real = 0  # number of real symbols (not DEFAULT) for this rule

        # Copy the expansion of the current rule
        for i_symbol in range(r_max_size_to_expand):
            symbol = production_array_to_expand[offset_to_expand + i_symbol]
            offset_expanded = offset_expanded_part + i_symbol * r_max_size

            # Copy the expansion of the current symbol of the current rule
            if symbol < nb_rule:   # symbol is a variable, must be to expand
                for i in range(r_max_size):
                    symb = production_array[symbol * r_max_size + i]
                    if symb != DEFAULT:  # real symbol
                        production_expanded_array[offset_expanded + i] = symb
                        nb_real += 1
                    else:                # the remain for this symbol must be DEFAULT
                        np.put(production_expanded_array,
                               range(offset_expanded + i, offset_expanded + r_max_size),
                               DEFAULT)

                        break
            elif symbol != DEFAULT:  # symbol is a constant
                nb_real += 1
                production_expanded_array[offset_expanded] = symbol
                np.put(production_expanded_array,
                       range(offset_expanded + 1, offset_expanded + r_max_size),
                       DEFAULT)
            else:                    # symbol is DEFAULT, the remain for this symbol also
                np.put(production_expanded_array,
                       range(offset_expanded,
                             offset_expanded_part + r_max_size_to_expand * r_max_size),
                       DEFAULT)

                break

        new_r_expanded_max_size = max(new_r_expanded_max_size, nb_real)

    # Concatenate real symbols for each rule
    new_i_symbol = 0
    for i_rule_expanded in range(nb_rule):
        remain = new_r_expanded_max_size
        for i_symbol in range(r_max_size_to_expand):
            offset_expanded = i_rule_expanded * r_expanded_max_size + i_symbol * r_max_size
            for i in range(r_max_size):
                symbol = production_expanded_array[offset_expanded + i]
                if symbol != DEFAULT:
                    production_expanded_array[new_i_symbol] = symbol
                    new_i_symbol += 1
                    remain -= 1
        if remain > 0:
            np.put(production_expanded_array,
                   range(new_i_symbol, new_i_symbol + remain),
                   DEFAULT)
            new_i_symbol += remain

    return new_r_expanded_max_size


def production_array_to_production(production_array, alphabet, nb_rule, array_rule_size,
                                   r_max_size):
    """
    Return a dictionary with production rules corresponding to production_array.

    Each rule must be in a part of length production_array // nb_rule.

    :param production_array: NumPy array of length at least nb_rule * r_max_size
    :param alphabet: str of symbols
    :param nb_rule: int > 0
    :param array_rule_size: int >= r_max_size
    :param r_max_size: int > 0 the biggest size of a rule from production

    :return: dict symbol: (str of symbols)
    """
    assert isinstance(production_array, np.ndarray), type(production_array)
    assert production_array.shape[0] >= nb_rule * r_max_size
    assert production_array.shape[0] % nb_rule == 0, (production_array.shape[0], nb_rule)

    assert isinstance(alphabet, str), type(alphabet)

    assert isinstance(nb_rule, int), type(nb_rule)
    assert nb_rule > 0, nb_rule

    assert isinstance(array_rule_size, int), type(array_rule_size)
    assert array_rule_size > 0, array_rule_size

    assert isinstance(r_max_size, int), type(r_max_size)
    assert r_max_size <= array_rule_size, r_max_size

    production = dict()

    for i_rule in range(nb_rule):
        symbol = alphabet[i_rule]
        first = array_rule_size * i_rule

        assert symbol not in production, symbol
        assert first < production_array.shape[0], (first, production_array.shape[0])

        production[symbol] \
            = alphabet_mapping_reverse(alphabet,
                                       production_array[first:first + r_max_size])

    return production


class ArrayLSystem(simple_l_system.LSystem):
    """
    Reimplementation of simple_l_system.LSystem
    with NumPy arrays instead "dynamic" used of strings.
    But as simple_l_system.FastLSystem
    the expand() method use production_iterate().

    The goal of that was to have the algorithms on a form
    more easily convertible in OpenCL
    and build classes to derive.
    """

    def __init__(self, alphabet, axiom, production, name=''):
        """
        Initialize the L-system (alphabet, axiom, production rules).

        Alphabet is maybe reordered to beginning by symbols of production rules.

        Production rule can't be constant.

        :param alphabet: str of unique symbols
        :param axiom: str of initial symbols
        :param production: dict symbol: (str of symbols)
        :param name: str
        """
        simple_l_system.LSystem.__init__(self, alphabet, axiom, production, name)

        variables, constants = simple_l_system.split_alphabet(self._alphabet, self._production)
        self._alphabet = variables + constants
        a_mapping = alphabet_mapping(self._alphabet)

        self._r_max_size = simple_l_system.rule_max_size(self._production)

        self._production_array = empty_production_array(len(self._production) * self._r_max_size)
        if __debug__:
            self._production_array.fill(42)
        copy_production_to_production_array(self._production, a_mapping, self._r_max_size,
                                            self._production_array)
        self._production_array.flags.writeable = False

    def expand(self, nb_iteration=1):
        """
        Expand and modify the current state nb_iteration times.

        :param nb_iteration: int >= 0
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration > 0:
            production_iter = self.production_iterate(nb_iteration)

            # Apply once this production rules equivalent to nb_iteration
            self._current = self.expand_string(self._current, production_iter)
            self._iteration += nb_iteration

    def production_iterate(self, nb_iteration):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        If nb_iteration == 0
        then return an empty dict.

        Computed in nb_iteration steps.

        :param nb_iteration: int >= 0

        :return: dict symbol: (str of symbols)
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration == 0:
            return dict()
        elif nb_iteration == 1:
            return dict(self._production)
        else:                    # Expand all production rules to be equivalent to nb_iteration
            nb_rule = len(self._production)
            r_max_size_to_expand = self._r_max_size
            r_expanded_max_size = r_max_size_to_expand * self._r_max_size

            r_max_nb_variable = simple_l_system.rule_max_nb_variable(self._production)
            r_final_expanded_max_size \
                = (simple_l_system.rule_iter_upper_size(nb_iteration - 1, r_max_nb_variable,
                                                        self._r_max_size) *
                   self._r_max_size)

            # Init arrays
            production_array_to_expand = empty_production_array(nb_rule * r_final_expanded_max_size)
            production_expanded_array = empty_production_array(nb_rule * r_final_expanded_max_size)

            for i in range(self._production_array.shape[0]):
                production_array_to_expand[i] = self._production_array[i]

            # Expand production rules (nb_iteration - 1) times with arrays
            for i in range(1, nb_iteration):
                new_r_expanded_max_size \
                    = production_array_expand(nb_rule, self._production_array, self._r_max_size,
                                              production_array_to_expand, r_max_size_to_expand,
                                              production_expanded_array, r_expanded_max_size)

                r_max_size_to_expand = new_r_expanded_max_size
                r_expanded_max_size = r_max_size_to_expand * self._r_max_size
                production_array_to_expand, production_expanded_array = production_expanded_array, production_array_to_expand

                assert r_expanded_max_size <= r_max_size_to_expand * self._r_max_size

            del production_expanded_array

            # Build dict of production rules equivalent to nb_iteration
            return production_array_to_production(production_array_to_expand,
                                                  self._alphabet, nb_rule,
                                                  r_max_size_to_expand,
                                                  r_max_size_to_expand)


class FastArrayLSystem(ArrayLSystem):
    """
    Reimplementation of ArrayLSystem
    with the same fast algorithm than fast_l_system.FastLSystem.
    """

    def production_iterate(self, nb_iteration):
        """
        Return a dictionary of production rules
        equivalent to expand nb_iteraton times self._production.

        If nb_iteration == 0
        then return an empty dict.

        Computed in nb_iteration steps.

        :param nb_iteration: int >= 0

        :return: dict symbol: (str of symbols)
        """
        assert isinstance(nb_iteration, int), type(nb_iteration)
        assert nb_iteration >= 0, nb_iteration

        if nb_iteration == 0:
            return dict()
        elif nb_iteration == 1:
            return dict(self._production)
        else:                    # Expand all production rules to be equivalent to nb_iteration
            nb_rule = len(self._production)
            r_max_size_to_expand = self._r_max_size
            r_expanded_max_size = r_max_size_to_expand * self._r_max_size

            r_max_nb_variable = simple_l_system.rule_max_nb_variable(self._production)
            r_final_expanded_max_size \
                = (simple_l_system.rule_iter_upper_size(nb_iteration // 2, r_max_nb_variable,
                                                        self._r_max_size)**2 *
                   self._r_max_size**(1 if nb_iteration % 2 == 0 else 2))

            # Init arrays
            production_array_to_expand = empty_production_array(nb_rule * r_final_expanded_max_size)
            production_expanded_array = empty_production_array(nb_rule * r_final_expanded_max_size)

            for i in range(self._production_array.shape[0]):
                production_array_to_expand[i] = self._production_array[i]

            # Binary representation without the most significant figure
            bins = bin(nb_iteration)[3:]

            # Expand production rules with arrays
            for bit in bins:
                # Expand by apply production rules to themselves
                new_r_expanded_max_size \
                    = production_array_expand(nb_rule, production_array_to_expand, r_max_size_to_expand,
                                              production_array_to_expand, r_max_size_to_expand,
                                              production_expanded_array, r_expanded_max_size)

                r_max_size_to_expand = new_r_expanded_max_size
                production_array_to_expand, production_expanded_array = production_expanded_array, production_array_to_expand

                if bit == '1':
                    r_expanded_max_size = r_max_size_to_expand * self._r_max_size

                    # Apply one more time the initial production
                    new_r_expanded_max_size \
                        = production_array_expand(nb_rule, self._production_array, self._r_max_size,
                                                  production_array_to_expand, r_max_size_to_expand,
                                                  production_expanded_array, r_expanded_max_size)

                    r_max_size_to_expand = new_r_expanded_max_size
                    production_array_to_expand, production_expanded_array = production_expanded_array, production_array_to_expand

                r_expanded_max_size = r_max_size_to_expand**2

            del production_expanded_array

            # Build dict of production rules equivalent to nb_iteration
            return production_array_to_production(production_array_to_expand,
                                                  self._alphabet, nb_rule,
                                                  r_max_size_to_expand,
                                                  r_max_size_to_expand)
