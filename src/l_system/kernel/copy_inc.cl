/* -*- coding: latin-1 -*- */
/**
 * Simple kernels to check OpenCL parameters
 * and to benchmark copy and inc numbers from a buffer to an other.
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * Olivier Pirson --- http://www.opimedia.be/
 * August 14, 2018
 */


/**
 * Put get_num_groups(0), get_global_size(0) and get_local_size(0) to sizes,
 * and get_group_id(0) in results[get_group_id(0)] for each work group.
 *
 * @param sizes buffer of length at least 3
 * @param results buffer of length at least get_num_groups(0)
 */
__kernel
void
check(__global unsigned int* sizes, __global unsigned int* results) {
  const unsigned int g_size = get_global_size(0);

  const unsigned int l_size = get_local_size(0);
  const unsigned int l_id = get_local_id(0);

  const unsigned int nb_work_group = get_num_groups(0);

  sizes[0] = nb_work_group;
  sizes[1] = g_size;
  sizes[2] = l_size;

  if (l_id == l_size - 1) {  // last work item set results[] value
    const unsigned int i_work_group = get_group_id(0);

    results[i_work_group] = i_work_group;
  }
}



/**
 * Copy each number from src to dest
 * and increment it.
 *
 * Each work unit copy and increment a range of numbers.
 *
 * @param src buffer of length at least buffer_size
 * @param buffer_size
 * @param dest buffer of length at least buffer_size
 */
__kernel
void
copy_inc(__global const unsigned char* const src, const unsigned int buffer_size,
         __global unsigned char* const dest) {
  const unsigned int range_size = buffer_size/get_global_size(0);
  const unsigned int offset = range_size*get_global_id(0);

  __global const unsigned char*  src_ptr = src + offset;
  __global const unsigned char* const src_end_ptr = src_ptr + range_size;

  __global unsigned char* dest_ptr = dest + offset;

  // #pragma unroll 8
  for ( ; src_ptr < src_end_ptr; ++dest_ptr, ++src_ptr) {
    *dest_ptr = *src_ptr + 1;
  }
}
