/* -*- coding: latin-1 -*- */
/**
 * Optimized OpenCL kernel for opt_turtle.OptOpenCLTurtleBitmap().
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * Olivier Pirson --- http://www.opimedia.be/
 * August 19, 2018
 */

#include "assert.cl"

/* *****************
 * Macro constants *
 *******************/

#ifndef ANGLE_INCREMENT
# define ANGLE_INCREMENT 0  // fake value, must be defined by host
#endif

#ifndef DISTANCE_INCREMENT
# define DISTANCE_INCREMENT 0  // fake value, must be defined by host
#endif



/* ********
 * Macros *
 **********/

/**
 * Table to convert
 *   'F' -> 0
 *   'f' -> 1
 *   '+' -> 2
 *   '-' -> 3
 * and other values to 4.
 */
#define DECLARE_CONVERT                                                 \
  const unsigned int convert[255] =                                     \
    {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2,  /* + */ \
     4, 3,  /* - */                                                     \
     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0,  /* F */ \
     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1,  /* f */ \
     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};


/**
 * Size of each part of l_changes.
 */
#define L_CHANGES_PART_SIZE 9


/**
 * Table to change position and angle
 * depending of the symbol converted by the array convert.
 */
#define DECLARE_L_CHANGES                                               \
  __local unsigned int l_changes[L_CHANGES_PART_SIZE*5];                \
                                                                        \
  if (get_local_id(0) == 0) {                                           \
    l_changes[ 0] = DISTANCE_INCREMENT;  /* x for each angle, for symbol F */ \
    l_changes[ 1] = 0;                                                  \
    l_changes[ 2] = -DISTANCE_INCREMENT;                                \
    l_changes[ 3] = 0;                                                  \
    l_changes[ 4] = 0;                   /* y for each angle, for symbol F */ \
    l_changes[ 5] = DISTANCE_INCREMENT;                                 \
    l_changes[ 6] = 0;                                                  \
    l_changes[ 7] = -DISTANCE_INCREMENT;                                \
    l_changes[ 8] = 0;                                                  \
                                                                        \
    l_changes[ 9] = DISTANCE_INCREMENT;  /* x for each angle, for symbol f */ \
    l_changes[10] = 0;                                                  \
    l_changes[11] = -DISTANCE_INCREMENT;                                \
    l_changes[12] = 0;                                                  \
    l_changes[13] = 0;                   /* y for each angle, for symbol f */ \
    l_changes[14] = DISTANCE_INCREMENT;                                 \
    l_changes[15] = 0;                                                  \
    l_changes[16] = -DISTANCE_INCREMENT;                                \
    l_changes[17] = 0;                                                  \
                                                                        \
    l_changes[18] = 0;                                                  \
    l_changes[19] = 0;                                                  \
    l_changes[20] = 0;                                                  \
    l_changes[21] = 0;                                                  \
    l_changes[22] = 0;                                                  \
    l_changes[23] = 0;                                                  \
    l_changes[24] = 0;                                                  \
    l_changes[25] = 0;                                                  \
    l_changes[26] = ANGLE_INCREMENT/90;  /* angle for each angle, for symbol + */ \
                                                                        \
    l_changes[27] = 0;                                                  \
    l_changes[28] = 0;                                                  \
    l_changes[29] = 0;                                                  \
    l_changes[30] = 0;                                                  \
    l_changes[31] = 0;                                                  \
    l_changes[32] = 0;                                                  \
    l_changes[33] = 0;                                                  \
    l_changes[34] = 0;                                                  \
    l_changes[35] = 4 - (ANGLE_INCREMENT/90);  /* angle for each angle for symbol - */ \
                                                                        \
    l_changes[36] = 0;                                                  \
    l_changes[37] = 0;                                                  \
    l_changes[38] = 0;                                                  \
    l_changes[39] = 0;                                                  \
    l_changes[40] = 0;                                                  \
    l_changes[41] = 0;                                                  \
    l_changes[42] = 0;                                                  \
    l_changes[43] = 0;                                                  \
    l_changes[44] = 0;                                                  \
  }



/* ************
 * Prototypes *
 **************/

/**
 * Return n/d rounded to the smallest integer greater than or equal.
 *
 * @param n != 0
 * @param d != 0
 *
 * @return ceil(n/d)
 */
unsigned int
posint_ceil(unsigned int n, unsigned int d);


/**
 * Return the position in the canvas corresponding to the position pixel (x, y).
 *
 * @param x
 * @param y
 * @param width > 0
 * @param height > 0
 *
 * @return the offset corresponding to the position (x, y) for an array width x height.
 */
unsigned int
pos_to_offset(const unsigned int x, const unsigned int y,
              const unsigned int width, const unsigned int height);



/* ***********
 * Functions *
 *************/

unsigned int
posint_ceil(unsigned int n, unsigned int d) {
  return (n - 1)/d + 1;
}


unsigned int
pos_to_offset(const unsigned int x, const unsigned int y,
              const unsigned int width, const unsigned int height) {
  return (height - 1 - y)*width + x;
  // inverse the y coordinate to correspond to the mathematical convention
}



/* ******
 * Main *
 ********/

/**
 * Each work unit draw in canvas the picture corresponding to a range of symbols of string
 * and initial position and angle stored in initials:
 *   [x, y, angle,
 *    x, y, angle,
 *    ...].
 *
 * @param string
 * @param size >= 1
 * @param distance_increment >= 1  (useless parameter because use of DISTANCE_INCREMENT macro)
 * @param angle_increment < 360  (useless parameter because use of ANGLE_INCREMENT macro)
 * @param width
 * @param height
 * @param initials
 * @param canvas
 */
__kernel
void
compute_draw(__global const unsigned char* string, const unsigned int size,
             const unsigned int distance_increment, const unsigned int angle_increment,
             const unsigned int width, const unsigned int height,
             __global const unsigned int* const initials,
             __global bool* const canvas
             ASSERT_DECLARE_PARAM) {
  assert_init;
  assert_v(size != 0, size);
  assert(distance_increment != 0);
  assert_v(distance_increment == DISTANCE_INCREMENT, distance_increment);
  assert_v(angle_increment < 360, angle_increment);
  assert_v(angle_increment == ANGLE_INCREMENT, angle_increment);

  DECLARE_L_CHANGES;
  barrier(CLK_LOCAL_MEM_FENCE);

  const unsigned int g_id = get_global_id(0);
  const unsigned int g_size = get_global_size(0);

  const unsigned int initials_offset = g_id*3;

  unsigned int x = initials[initials_offset];
  unsigned int y = initials[initials_offset + 1];
  unsigned int angle = initials[initials_offset + 2]/90;

  assert_v(x < width, x);
  assert_v(y < height, y);
  assert_v(angle < 4, angle);

  const unsigned int tmp_range_size = posint_ceil(size, g_size);
  const unsigned int offset = g_id*tmp_range_size;

  if (offset < size) {
    DECLARE_CONVERT;

    unsigned int range_size = min(tmp_range_size,
                                  size - offset);  // for last ranges

    assert(offset + range_size <= size);

    string += offset;
    while (range_size--) {
      const unsigned int l_changes_offset = convert[*(string++)]*L_CHANGES_PART_SIZE;
      const unsigned int l_changes_offset_angle = l_changes_offset + angle;

      const unsigned int prev_x = x;
      x += l_changes[l_changes_offset_angle];

      const unsigned int prev_y = y;
      y += l_changes[l_changes_offset_angle + 4];

      angle = (angle + l_changes[l_changes_offset + 8]) & 3;  // modulo 4

      if (!l_changes_offset) {  // if symbol F (converted in 0)
        const unsigned int k = angle & 1;  // 0 for 0 or 180 degrees; 1 for 90 or 270 degrees

        const unsigned int pos0 = (k
                                   ? min(prev_y, y)
                                   : min(prev_x, x));

        const unsigned int diff = select(1u, -width, k);  // (k ? -width : 1);

        /*
          This faster version works fine on my computer, both on GPU and CPU,
          but crash on "ranger", both on GPU and CPU!

        __global bool* ptr = canvas + select(pos_to_offset(pos0, y, width, height),
                                             pos_to_offset(x, pos0, width, height),
                                             k);

#pragma unroll
        for (unsigned int i = 0; i <= DISTANCE_INCREMENT; ptr += diff, ++i) {
          *ptr = 0;
        }

          But the following works fine everywhere! WTF!!!
        */

        unsigned int draw_offset = select(pos_to_offset(pos0, y, width, height),
                                          pos_to_offset(x, pos0, width, height),
                                          k);
        //                         (k
        //                          ? pos_to_offset(x, y0, width, height)
        //                          : pos_to_offset(x0, y, width, height))

#pragma unroll
        for (unsigned int i = 0; i <= DISTANCE_INCREMENT; draw_offset += diff, ++i) {
          canvas[draw_offset] = 0;
        }
      }
    }
  }

  assert_v(x < width, x);
  assert_v(y < height, y);
  assert_v(angle < 360, angle);
}



/**
 * Each work unit compute the minimal and maximal positions drawn for a range of symbols of string
 * and store them in result with final position and angle for this range:
 *   [x_min, y_min, x_max, y_max, x, y, angle,
 *    x_min, y_min, x_max, y_max, x, y, angle,
 *    ...].
 *
 * @param string
 * @param size >= 1
 * @param distance_increment >= 1  (useless parameter because use of DISTANCE_INCREMENT macro)
 * @param angle_increment < 360  (useless parameter because use of ANGLE_INCREMENT macro)
 * @param result
 */
__kernel
void
compute_size(__global const unsigned char* string, const unsigned int size,
             const unsigned int distance_increment, const unsigned int angle_increment,
             __global signed int* const result
             ASSERT_DECLARE_PARAM) {
  assert_init;
  assert_v(size != 0, size);
  assert(distance_increment != 0);
  assert_v(distance_increment == DISTANCE_INCREMENT, distance_increment);
  assert_v(angle_increment < 360, angle_increment);
  assert_v(angle_increment == ANGLE_INCREMENT, angle_increment);

  DECLARE_L_CHANGES;
  barrier(CLK_LOCAL_MEM_FENCE);

  signed int x = 0;
  signed int x_min = x;
  signed int x_max = x;

  signed int y = 0;
  signed int y_min = y;
  signed int y_max = y;

  signed int angle = 0;

  const unsigned int g_id = get_global_id(0);
  const unsigned int tmp_range_size = posint_ceil(size, get_global_size(0));
  const unsigned int offset = g_id*tmp_range_size;

  if (offset < size) {
    DECLARE_CONVERT;

    unsigned int range_size = min(tmp_range_size,
                                  size - offset);  // for last ranges

    assert(offset + range_size <= size);

    string += offset;
    while (range_size--) {
      const unsigned int l_changes_offset = convert[*(string++)]*L_CHANGES_PART_SIZE;

      x += l_changes[l_changes_offset + angle];
      x_min = min(x_min, x);
      x_max = max(x_max, x);

      y += l_changes[l_changes_offset + 4 + angle];
      y_min = min(y_min, y);
      y_max = max(y_max, y);

      angle = (angle + l_changes[l_changes_offset + 8]) & 3;  // modulo 4
    }
  }

  const unsigned int result_offset = g_id*7;

  result[result_offset    ] = x_min;
  result[result_offset + 1] = y_min;
  result[result_offset + 2] = x_max;
  result[result_offset + 3] = y_max;

  result[result_offset + 4] = x;         // final x
  result[result_offset + 5] = y;         // final y
  result[result_offset + 6] = angle*90;  // final angle
}
