/* -*- coding: latin-1 -*- */
/**
 * OpenCL kernel for opencl_turtle.OpenCLTurtleBitmap().
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * Olivier Pirson --- http://www.opimedia.be/
 * August 18, 2018
 */

#include "assert.cl"



/* ************
 * Prototypes *
 **************/

/**
 * Draw in canvas a horizontal line from (x1, y) to (x2, y).
 *
 * @param x1 <= x2
 * @param x2 < width
 * @param y
 * @param width
 * @param height
 * @param canvas
 */
void
draw_line_horizontal(const unsigned int x1, const unsigned int x2,
                     const unsigned int y,
                     const unsigned int width, const unsigned int height,
                     __global bool* const canvas
                     ASSERT_DECLARE_PARAM);


/**
 * Draw in canvas a vertical line from (x, y1) to (x, y2).
 *
 * @param x
 * @param y1 <= y2
 * @param y2 < height
 * @param width
 * @param height
 * @param canvas
 */
void
draw_line_vertical(const unsigned int x,
                   const unsigned int y1, const unsigned int y2,
                   const unsigned int width, const unsigned int height,
                   __global bool* const canvas
                   ASSERT_DECLARE_PARAM);


/**
 * Return n/d rounded to the smallest integer greater than or equal.
 *
 * @param n != 0
 * @param d != 0
 *
 * @return ceil(n/d)
 */
unsigned int
posint_ceil(unsigned int n, unsigned int d);


/**
 * Return the position in the canvas corresponding to the position pixel (x, y).
 *
 * @param x
 * @param y
 * @param width > 0
 * @param height > 0
 *
 * @return the offset corresponding to the position (x, y) for an array width x height.
 */
unsigned int
pos_to_offset(const unsigned int x, const unsigned int y,
              const unsigned int width, const unsigned int height);



/* ***********
 * Functions *
 *************/

void
draw_line_horizontal(const unsigned int x0, const unsigned int x1,
                     const unsigned int y,
                     const unsigned int width, const unsigned int height,
                     __global bool* const canvas
                     ASSERT_DECLARE_PARAM) {
  assert(x0 <= x1);
  assert(x1 < width);

  __global bool* ptr = canvas + pos_to_offset(x0, y, width, height);
  unsigned int nb = x1 + 1 - x0;

  for ( ; nb != 0; ++ptr, --nb) {
    *ptr = 0;
  }
}


void
draw_line_vertical(const unsigned int x,
                   const unsigned int y0, const unsigned int y1,
                   const unsigned int width, const unsigned int height,
                   __global bool* const canvas
                   ASSERT_DECLARE_PARAM) {
  assert(y0 <= y1);
  assert(y1 < height);

  __global bool* ptr = canvas + pos_to_offset(x, y0, width, height);
  unsigned int nb = y1 + 1 - y0;

  for ( ; nb != 0; ptr -= width, --nb) {
    *ptr = 0;
  }
}


unsigned int
posint_ceil(unsigned int n, unsigned int d) {
  return (n - 1)/d + 1;
}


unsigned int
pos_to_offset(const unsigned int x, const unsigned int y,
              const unsigned int width, const unsigned int height) {
  return (height - 1 - y)*width + x;
  // inverse the y coordinate to correspond to the mathematical convention
}



/* ******
 * Main *
 ********/

/**
 * Each work unit draw in canvas the picture corresponding to a range of symbols of string
 * and initial position and angle stored in initials:
 *   [x, y, angle,
 *    x, y, angle,
 *    ...].
 *
 * @param string
 * @param size >= 1
 * @param distance_increment >= 1
 * @param angle_increment < 360
 * @param width
 * @param height
 * @param initials
 * @param canvas
 */
__kernel
void
compute_draw(__global const unsigned char* const string, const unsigned int size,
             const unsigned int distance_increment, const unsigned int angle_increment,
             const unsigned int width, const unsigned int height,
             __global const unsigned int* const initials,
             __global bool* const canvas
             ASSERT_DECLARE_PARAM) {
  assert_init;
  assert_v(size != 0, size);
  assert(distance_increment != 0);
  assert_v(angle_increment < 360, angle_increment);

  const unsigned int g_id = get_global_id(0);
  const unsigned int g_size = get_global_size(0);

  const unsigned int initials_offset = g_id*3;

  unsigned int x = initials[initials_offset];
  unsigned int y = initials[initials_offset + 1];
  unsigned int angle = initials[initials_offset + 2];

  assert_v(x < width, x);
  assert_v(y < height, y);
  assert_v(angle < 360, angle);

  const unsigned int tmp_range_size = posint_ceil(size, g_size);
  const unsigned int offset = g_id*tmp_range_size;

  if (offset < size) {
    const unsigned int range_size = min(tmp_range_size,
                                        size - offset);  // for last ranges

    assert(offset + range_size <= size);

    for (unsigned int i = 0; i < range_size; ++i) {
      const unsigned char symbol = string[offset + i];

      switch (symbol) {
      case 'F':
        {
          const unsigned int prev_x = x;
          const unsigned int prev_y = y;

          switch (angle) {
          case 0:
            x += distance_increment;
            draw_line_horizontal(prev_x, x, y, width, height, canvas ASSERT_PARAM);

            break;

          case 180:
            x -= distance_increment;
            draw_line_horizontal(x, prev_x, y, width, height, canvas ASSERT_PARAM);

            break;

          case 90:
            y += distance_increment;
            draw_line_vertical(x, prev_y, y, width, height, canvas ASSERT_PARAM);

            break;

          case 270:
            y -= distance_increment;
            draw_line_vertical(x, y, prev_y, width, height, canvas ASSERT_PARAM);

            break;
          }
        }

        break;

      case 'f':
        switch (angle) {
        case 0:
        case 180:
          x += (angle == 0
                ? distance_increment
                : -distance_increment);

          break;

        case 90:
        case 270:
          y += (angle == 90
                ? distance_increment
                : -distance_increment);

          break;
        }

        break;

      case '+':  // left
        angle = (angle + angle_increment) % 360;

        break;

      case '-':  // right
        angle = (angle + 360 - angle_increment) % 360;

        break;
      }
    }
  }

  assert_v(x < width, x);
  assert_v(y < height, y);
  assert_v(angle < 360, angle);
}



/**
 * Each work unit compute the minimal and maximal positions drawn for a range of symbols of string
 * and store them in result with final position and angle for this range:
 *   [x_min, y_min, x_max, y_max, x, y, angle,
 *    x_min, y_min, x_max, y_max, x, y, angle,
 *    ...].
 *
 * @param string
 * @param size >= 1
 * @param distance_increment >= 1
 * @param angle_increment < 360
 * @param result
 */
__kernel
void
compute_size(__global const unsigned char* const string, const unsigned int size,
             const unsigned int distance_increment, const unsigned int angle_increment,
             __global signed int* const result
             ASSERT_DECLARE_PARAM) {
  assert_init;
  assert_v(size != 0, size);
  assert(distance_increment != 0);
  assert_v(angle_increment < 360, angle_increment);

  signed int x = 0;
  signed int x_min = x;
  signed int x_max = x;

  signed int y = 0;
  signed int y_min = y;
  signed int y_max = y;

  signed int angle = 0;

  const unsigned int g_id = get_global_id(0);
  const unsigned int g_size = get_global_size(0);
  const unsigned int tmp_range_size = posint_ceil(size, g_size);
  const unsigned int offset = g_id*tmp_range_size;

  if (offset < size) {
    const unsigned int range_size = min(tmp_range_size,
                                        size - offset);  // for last ranges

    assert(offset + range_size <= size);

    for (unsigned int i = 0; i < range_size; ++i) {
      const unsigned char symbol = string[offset + i];

      switch (symbol) {
      case 'F':  // forward
      case 'f':
        switch (angle) {
        case 0:
        case 180:
          x += (angle == 0
                ? distance_increment
                : -distance_increment);
          x_min = min(x_min, x);
          x_max = max(x_max, x);

          break;

        case 90:
        case 270:
          y += (angle == 90
                ? distance_increment
                : -distance_increment);
          y_min = min(y_min, y);
          y_max = max(y_max, y);

          break;
        }

        break;

      case '+':  // left
        angle = (angle + angle_increment) % 360;

        break;

      case '-':  // right
        angle = (angle + 360 - angle_increment) % 360;

        break;
      }
    }
  }

  const unsigned int result_offset = g_id*7;

  result[result_offset    ] = x_min;
  result[result_offset + 1] = y_min;
  result[result_offset + 2] = x_max;
  result[result_offset + 3] = y_max;

  result[result_offset + 4] = x;      // final x
  result[result_offset + 5] = y;      // final y
  result[result_offset + 6] = angle;  // final angle
}
