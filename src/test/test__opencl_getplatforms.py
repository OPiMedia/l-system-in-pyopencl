#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Simple pytest file to exhibit bug on the supercomputer "ranger" from VUB

pyopencl.get_platforms() crash by "Segmentation fault (core dumped)"
when it is run from pytest.

* Python version: 2.7.12 (default, Dec  4 2017, 14:50:18)  [GCC 5.4.0 20160609]
* PyOpenCL version: 2018.1.1
* pytest 3.5.1


The invocation by pytest crash on the supercomputer "ranger" (but works on my computer):
$ pytest test/test__opencl_getplatforms.py
>>>
================================================================================== test session starts ==================================================================================
platform linux2 -- Python 2.7.12, pytest-3.5.1, py-1.5.3, pluggy-0.6.0
rootdir: /home/gortega/opi/l/src, inifile:
collected 1 item

test/test__opencl_getplatforms.py Segmentation fault (core dumped)
<<<


The direct invocation of this file works also on the supercomputer "ranger":
$ python ./test/test__opencl_getplatforms.py
>>>
test__get_platforms ...
===== done =====
<<<


:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 22, 2018
"""

from __future__ import print_function

import sys

import pyopencl as cl

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)


def test__get_platforms():
    """
    Only try to get platforms list from PyOpenCL.

    This fundamental command crash on the supercomputer "ranger"
    when it is run from pytest!!!
    """
    platforms = cl.get_platforms()

    assert len(platforms) >= 2


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
