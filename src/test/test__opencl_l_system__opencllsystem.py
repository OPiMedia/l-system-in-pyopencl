#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests of class OpenCLLSystem from l_system.opencl_l_system.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import glob
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import test

import l_system.simple_l_system as simple_l_system
import l_system.fast_l_system as fast_l_system

import l_system.opencl_l_system as opencl_l_system


OPENCL_DEBUG = False


def test__expand_algae():
    """
    Compare the expansion of the algae L-system
    to the result from the Wikipedia page.
    """
    production = {'A': 'AB',
                  'B': 'A'}
    algae = opencl_l_system.OpenCLLSystem('AB', 'A', production, debug=OPENCL_DEBUG)

    assert str(algae) == 'A'

    for i, result in enumerate(test.ALGAE):
        assert algae.iteration() == i
        assert str(algae) == result

        if i < len(test.ALGAE) - 1:
            assert algae.expand_string(str(algae), production) == test.ALGAE[i + 1]
            assert algae.iteration() == i
            assert str(algae) == result

        algae.expand()


def test__production_iterate():
    """
    Compare the result of OpenCLLSystem.production_iterate(nb_iteration)
    with the results of FastLSystem.production_iterate(nb_iteration).
    """
    devices = [opencl_l_system.get_device(i, 0) for i in range(2)]

    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()

        assert simple_l_system.is_production(production), production

        for nb_iteration in range(7):
            lsystem2 = fast_l_system.FastLSystem(alphabet, axiom, production)
            production_iter2 = lsystem2.production_iterate(nb_iteration)
            result2 = lsystem2.expand_string(lsystem2._axiom, production_iter2)

            for device in devices:
                for nb_work_item in (2, 3, 4, 5, 6, 7, 8, 31, 32, 33, 256):
                    if not opencl_l_system.device_is_ok(device, 1, nb_work_item):
                        break

                    lsystem = opencl_l_system.OpenCLLSystem(alphabet, axiom, production,
                                                            device=device,
                                                            nb_work_item=nb_work_item,
                                                            debug=OPENCL_DEBUG)
                    production_iter = lsystem.production_iterate(nb_iteration)
                    result = lsystem.expand_string(lsystem._axiom, production_iter)

                    assert result == result2
                    assert production_iter == production_iter2


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
