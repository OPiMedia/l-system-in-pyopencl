#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests of load_l_system() and class LSystem from l_system.simple_l_system.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import glob
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import test

import l_system.simple_l_system as simple_l_system


def test__expand_algae():
    """
    Compare the expansion of the algae L-system
    to the result from the Wikipedia page.
    """
    production = {'A': 'AB',
                  'B': 'A'}
    algae = simple_l_system.LSystem('AB', 'A', production)

    assert str(algae) == 'A'

    for i, result in enumerate(test.ALGAE):
        assert algae.iteration() == i
        assert str(algae) == result

        if i < len(test.ALGAE) - 1:
            assert algae.expand_string(str(algae), production) == test.ALGAE[i + 1]
            assert algae.iteration() == i
            assert str(algae) == result

        algae.expand()


def test__expand_example_files():
    """
    Compare the expansion of the test.EXAMPLES L-systems
    to the results from the Wikipedia page.
    """
    for key_name, results in sorted(test.EXAMPLES.items()):
        filename = test.EXAMPLES_DIR + key_name.replace(' ', '_') + '.txt'
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)

        assert name == key_name

        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()
        lsystem = simple_l_system.LSystem(alphabet, axiom, production)

        assert str(lsystem) == axiom
        assert str(lsystem) == results[0]

        for i, result in enumerate(results):
            assert lsystem.iteration() == i
            assert str(lsystem) == result

            if i < len(results) - 1:
                assert lsystem.expand_string(str(lsystem),
                                             production) == results[i + 1]
                assert lsystem.iteration() == i
                assert str(lsystem) == result

            lsystem.expand()


def test__expand_step():
    """
    Compare the expansion of several iterations in one step
    to the expansion iteration by iteration.

    Compare also the results of rule_max_nb_variable() and rule_max_size().
    """
    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()

        r_max_size = simple_l_system.rule_max_size(production)
        r_max_nb_variable = simple_l_system.rule_max_nb_variable(production)

        found_exact_r_max_size = False
        found_exact_r_max_nb_variable = False
        for rule in production.values():
            exact_r_max_size = len(rule)
            exact_r_max_nb_variable = len([None for symbol in rule
                                           if symbol in production])
            found_exact_r_max_size = (found_exact_r_max_size or
                                      (r_max_size == exact_r_max_size))
            found_exact_r_max_nb_variable = (found_exact_r_max_nb_variable or
                                             (r_max_nb_variable == exact_r_max_nb_variable))

            assert r_max_size >= exact_r_max_size
            assert r_max_nb_variable >= exact_r_max_nb_variable

        assert found_exact_r_max_size
        assert found_exact_r_max_nb_variable

        for size_step in range(10):
            lsystem = simple_l_system.LSystem(alphabet, axiom, production)

            assert str(lsystem) == axiom

            for _ in range(size_step):
                lsystem.expand()

            result = str(lsystem)

            lsystem2 = simple_l_system.LSystem(alphabet, axiom, production)
            assert str(lsystem2) == axiom
            lsystem2.expand(size_step)

            assert result == str(lsystem2)
            assert lsystem.iteration() == lsystem2.iteration()

            if name == 'dragon curve':
                # Check the first half property of dragon curve
                if size_step > 0:
                    lsystem_prev = simple_l_system.LSystem(alphabet, axiom, production)
                    lsystem_prev.expand(size_step - 1)
                    prev = str(lsystem_prev)
                    current = str(lsystem)

                    assert current[:len(current) // 2] == prev + '+'

                # Check the number of symbols of dragon curve
                n = size_step
                if n > 0:
                    assert len(str(lsystem)) == 2**(n + 2) - 2
                    assert len([None for symbol in str(lsystem) if symbol == 'X']) == 2**(n - 1)
                    assert len([None for symbol in str(lsystem) if symbol == 'Y']) == 2**(n - 1)
                    assert len([None for symbol in str(lsystem) if symbol == '+']) == 2**n
                    assert len([None for symbol in str(lsystem) if symbol == '-']) == 2**n - 2
                assert len([None for symbol in str(lsystem) if symbol == 'F']) == 2**n

                # For dragon curve the upper bounds are exact
                r_max_nb_variable = simple_l_system.rule_max_nb_variable(production)
                r_max_nb_constant = simple_l_system.rule_max_nb_constant(alphabet, production)
                r_max_size = simple_l_system.rule_max_size(production)

                variables, constants = simple_l_system.split_alphabet(alphabet, production)
                a_nb_variable = len([None for symbol in axiom if symbol in variables])
                a_nb_constant = len([None for symbol in axiom if symbol in constants])
                a_size = len(axiom)

                assert a_nb_variable + a_nb_constant == a_size

                s_nb_variable = len([None for symbol in result if symbol in variables])
                s_nb_constant = len([None for symbol in result if symbol in constants])
                s_size = len(result)

                assert s_nb_variable + s_nb_constant == s_size

                s_bound_nb_variable, s_bound_nb_constant, s_bound_size \
                    = simple_l_system.string_iter_upper_bounds(size_step,
                                                               r_max_nb_variable, r_max_nb_constant,
                                                               r_max_size,
                                                               a_nb_variable, a_nb_constant,
                                                               a_size)

                assert s_bound_nb_variable == s_nb_variable
                assert s_bound_nb_constant == s_nb_constant
                assert s_bound_size == s_size

            elif name == 'fractal binary tree':
                # Check the number of symbols of fractal binary tree
                n = size_step
                assert len(str(lsystem)) == 2**(n - 1) * (n + 6) - 2
                assert len([None for symbol in str(lsystem) if symbol == '0']) == 2**n
                assert len([None for symbol in str(lsystem) if symbol == '1']) == 2**(n - 1) * n
                assert len([None for symbol in str(lsystem) if symbol == '[']) == 2**n - 1
                assert len([None for symbol in str(lsystem) if symbol == ']']) == 2**n - 1


def test__production_iterate():
    """
    Compare the result of LSystem.production_iterate(nb_iteration)
    with the results of LSystem.expand(nb_iteration) on each symbol.

    Compare also the results of several functions about size and upper bounds.
    """
    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()

        assert simple_l_system.is_production(production), production

        r_max_nb_variable = simple_l_system.rule_max_nb_variable(production)
        r_max_nb_constant = simple_l_system.rule_max_nb_constant(alphabet, production)
        r_max_size = simple_l_system.rule_max_size(production)

        assert r_max_nb_variable > 0
        assert r_max_size > 0
        assert r_max_nb_variable + r_max_nb_constant >= r_max_size, \
            (r_max_nb_variable, r_max_nb_constant, r_max_size)

        found_exact_r_max_nb_variable = False
        found_exact_r_max_nb_constant = False
        found_exact_r_max_size = False
        for rule in production.values():
            exact_r_max_nb_variable = len([None for symbol in rule
                                           if symbol in production])
            exact_r_max_nb_constant = len([None for symbol in rule
                                           if symbol not in production])
            exact_r_max_size = len(rule)

            found_exact_r_max_nb_variable = (found_exact_r_max_nb_variable or
                                             (r_max_nb_variable == exact_r_max_nb_variable))
            found_exact_r_max_nb_constant = (found_exact_r_max_nb_constant or
                                             (r_max_nb_constant == exact_r_max_nb_constant))
            found_exact_r_max_size = (found_exact_r_max_size or
                                      (r_max_size == exact_r_max_size))

            assert r_max_nb_variable >= exact_r_max_nb_variable
            assert r_max_nb_constant >= exact_r_max_nb_constant
            assert r_max_size >= exact_r_max_size

        assert found_exact_r_max_nb_variable
        assert found_exact_r_max_nb_constant
        assert found_exact_r_max_size

        for nb_iteration in range(10):
            lsystem = simple_l_system.LSystem(alphabet, axiom, production)
            production_iter = lsystem.production_iterate(nb_iteration)

            for symbol in alphabet:
                lsystem = simple_l_system.LSystem(alphabet, symbol, production)
                lsystem.expand(nb_iteration)

                assert production_iter.get(symbol, symbol) == str(lsystem)

            ri_upper_nb_variable = simple_l_system.rule_iter_upper_nb_variable(nb_iteration, r_max_nb_variable)
            ri_upper_nb_constant = simple_l_system.rule_iter_upper_nb_constant(nb_iteration, r_max_nb_variable, r_max_nb_constant)
            ri_upper_size = simple_l_system.rule_iter_upper_size(nb_iteration, r_max_nb_variable, r_max_size)

            assert ri_upper_nb_variable + ri_upper_nb_constant == ri_upper_size
            assert simple_l_system.rule_iter_upper_bounds(nb_iteration, r_max_nb_variable, r_max_nb_constant, r_max_size) \
                == (ri_upper_nb_variable, ri_upper_nb_constant, ri_upper_size)

            for rule in production.values():
                if nb_iteration == 0:
                    nb_variable = 1
                    nb_constant = 0
                else:
                    nb_variable = len([None for symbol in rule
                                       if symbol in production])
                    nb_constant = len([None for symbol in rule
                                       if symbol not in production])

                assert nb_variable <= ri_upper_nb_variable
                assert nb_constant <= ri_upper_nb_constant
                assert nb_variable + nb_constant <= ri_upper_size


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
