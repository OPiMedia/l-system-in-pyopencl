#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Benchmarks implementations of building image from L-systems.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 19, 2018
"""

from __future__ import division
from __future__ import print_function

import gc
import sys
import time

import numpy as np

import l_system.fast_l_system
import l_system.turtle
import l_system.opencl_turtle
import l_system.opt_opencl_turtle


def f(x):
    """
    :param x: float

    :return: str
    """
    assert isinstance(x, float), type(x)

    return '{:.6f}'.format(x)


def fs(seq):
    """
    :param seq: list of float

    :return: str
    """
    assert isinstance(seq, list), type(seq)

    return '[' + ', '.join([f(x) for x in seq]) + ']'


def li(x):
    """
    :param x: int or long

    :return: str
    """
    return str(x).replace('L', '')


def lis(seq):
    """
    :param seq: list of (int or long)

    :return: str
    """
    assert isinstance(seq, list), type(seq)

    return '[' + ', '.join([li(x) for x in seq]) + ']'


########
# Main #
########
def main():
    """
    Benchmarks implementations of L-system expansion.
    """
    filename = '../systems/dragon_curve.txt'
    mapping_filename = '../systems/turtle/turtle_dragon_curve.txt'
    implementations = (l_system.turtle.TurtleBitmap,
                       l_system.opencl_turtle.OpenCLTurtleBitmap,
                       l_system.opt_opencl_turtle.OptOpenCLTurtleBitmap)
    nb_skip = 3
    nb_repetition = 10

    name, alphabet, axiom, production = l_system.simple_l_system.load_l_system(filename)

    angle_increment, mapping = l_system.turtle.load_turtle_mapping(mapping_filename)
    distance_increment = 8.0
    angle_initial = 90.0
    margin = 5.0

    python_version = 'Python version: {}{}'.format(sys.version.replace('\n', ' '),
                                                   (' DEBUG MODE' if __debug__
                                                    else ''))
    print('# iteration\tlength\twidth\theight\timplementation\tdevice\tGPU?\t# work group\t# work item\ttotal time\ttotal opencl\t# correct\taverage\taverage opencl\ttimes\ttimes opencl',
          name, nb_skip, nb_repetition, python_version,
          sep='\t')
    print('# iteration\tlength\twidth\theight\timplementation\tdevice\tGPU?\t# work group\t# work item\trepetition\tsucceed?\terror',
          name, nb_skip, nb_repetition, python_version,
          sep='\t', file=sys.stderr)

    for nb_iteration in range(21):  # for each number of iterations
        lsystem = l_system.fast_l_system.FastLSystem(alphabet, axiom, production, name)
        lsystem.expand(nb_iteration)
        string = str(lsystem)
        len_string = len(string)

        turt2 = l_system.turtle.TurtleBitmap(string, distance_increment,
                                             angle_increment, angle_initial,
                                             mapping,
                                             margin)
        del lsystem

        turt2.process()
        correct_canvas = turt2.canvas()
        width = turt2.intwidth()
        height = turt2.intheight()
        # turt2.save_image('benchmarks_turtle2.png')

        gc.collect()

        for ImplTurtle in implementations:  # for each implementation
            is_opencl = ('OpenCL' in ImplTurtle.__name__)
            for platform_id in range(2 if is_opencl else 1):  # just 1 or for each OpenCL platform
                if is_opencl:
                    device = l_system.opencl_l_system.get_device(platform_id, 0)
                    gpu = ('GPU' if l_system.opencl_l_system.device_is_nvidia(device) else 'CPU')
                else:
                    gpu = 'CPU'

                for k in range(11 if is_opencl else 1):  # for 1 process or several nb of work groups
                    nb_work_group = 2**k  # 1 or 1, 4, 16 ... 1024
                    for l in range(0, 11 if is_opencl else 1, 2):  # for 1 process or several nb of work items
                        nb_work_item = 2**l  # 1 or 1, 2, 4, 8, 16 ... 1024
                        if is_opencl and not l_system.opencl_l_system.device_is_ok(device, nb_work_group, nb_work_item):
                            break
                        if ('OptOpenCL' in ImplTurtle.__name__) and (gpu == 'CPU') and (nb_work_item == 1):
                            break

                        turt = None
                        print(nb_iteration, len_string, width, height,
                              ImplTurtle.__name__,
                              platform_id, gpu, nb_work_group, nb_work_item, sep='\t', end='\t')
                        sys.stdout.flush()

                        for i in range(nb_skip + nb_repetition):  # for each iteration
                            is_skip = (i < nb_skip)
                            if i == nb_skip:
                                durations_s = []
                                cl_durations_ns = []

                            duration_s = 0.0
                            turt = None

                            start = time.time()
                            try:
                                turt = (ImplTurtle(string, distance_increment,
                                                   angle_increment, angle_initial,
                                                   mapping,
                                                   margin,
                                                   device=device,
                                                   nb_work_group=nb_work_group, nb_work_item=nb_work_item)
                                        if is_opencl
                                        else ImplTurtle(string, distance_increment,
                                                        angle_increment, angle_initial,
                                                        mapping,
                                                        margin))
                                turt.process()

                            except Exception as ex:
                                print(nb_iteration, len_string, width, height,
                                      ImplTurtle.__name__,
                                      platform_id, gpu, nb_work_group, nb_work_item, i,
                                      'FAILED!', ex,
                                      sep='\t', file=sys.stderr)
                                sys.stderr.flush()

                            end = time.time()

                            # Check that the result is exactly the same
                            # or if it is the same with a additional blank line due to rounding
                            is_correct = ((turt is not None) and
                                          (np.array_equal(turt.canvas(), correct_canvas) or
                                           np.array_equal(turt.canvas(), np.delete(correct_canvas, -1, 1))))
                            if is_correct:
                                print(nb_iteration, len_string, width, height,
                                      ImplTurtle.__name__,
                                      platform_id, gpu, nb_work_group, nb_work_item, i,
                                      ('skipped' if is_skip else 'succeed'),
                                      sep='\t', file=sys.stderr)
                                sys.stderr.flush()
                            else:
                                print(nb_iteration, len_string, width, height,
                                      ImplTurtle.__name__,
                                      platform_id, gpu, nb_work_group, nb_work_item, i,
                                      ('INCORRECT!'),
                                      sep='\t', file=sys.stderr)
                                sys.stderr.flush()
                            if not is_skip:
                                if is_correct:
                                    duration_s = end - start
                                    durations_s.append(duration_s)
                                    if is_opencl:
                                        cl_duration_ns = turt.cl_duration_ns()
                                        cl_durations_ns.append(cl_duration_ns)
                                    else:
                                        cl_durations_ns.append(0)

                            # turt.save_image('benchmarks_turtle.png')
                            if False:
                                if not is_correct:
                                    img_filename = ('benchmarks_turtle_{}_{}_{}_{}_{}_{}'
                                                    .format(nb_iteration,
                                                            ImplTurtle.__name__,
                                                            platform_id, nb_work_group, nb_work_item, i))
                                    turt2.save_image(img_filename + '_correct.png')
                                    turt.save_image(img_filename + '_incorrect.png')

                        del turt

                        nb = len(durations_s)
                        total_duration_s = float(sum(durations_s))
                        average_s = (total_duration_s / nb if nb != 0 else 0.0)
                        if is_opencl:
                            cl_total_duration_ns = sum(cl_durations_ns)
                            cl_average_ns = (float(cl_total_duration_ns) / nb if nb != 0 else 0.0)
                        else:
                            cl_total_duration_ns = 0
                            cl_average_ns = 0.0
                        print(f(total_duration_s), li(cl_total_duration_ns),
                              nb,
                              f(average_s), f(cl_average_ns),
                              fs(durations_s), lis(cl_durations_ns), sep='\t')
                        sys.stdout.flush()


if __name__ == '__main__':
    main()
