Python version: 2.7.12 (default, Dec  4 2017, 14:50:18)  [GCC 5.4.0 20160609]
PyOpenCL version: 2018.1.1
2 OpenCL platforms
========================================
Platform name: "Intel(R) OpenCL"
         vendor: "Intel(R) Corporation"
         version: "OpenCL 2.0 "
         profile: "FULL_PROFILE"
         extensions: "cl_khr_icd cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_byte_addressable_store cl_khr_depth_images cl_khr_3d_image_writes cl_intel_exec_by_local_thread cl_khr_spir cl_khr_fp64 cl_khr_image2d_from_buffer"
  1 device
  0:0 ------------------------------
  Device name: "Intel(R) Core(TM) i7-6900K CPU @ 3.20GHz"
         global mem cache size: 262144 o = 256 kio
         global mem size: 101281853440 o = 98908060 kio = 96589 Mio = 94 Gio
         local mem size: 32768 o = 32 kio
         max clock frequency: 3200
         max compute units: 16
         max constant buffer size: 131072 o = 128 kio
         max mem alloc size: 25320463360 o = 24727015 kio = 24147 Mio = 23 Gio
         max work group size: 8192 o = 8 kio
         max work item sizes: 8192 8192 8192
         opencl c version: "OpenCL C 2.0 "
========================================
Platform name: "NVIDIA CUDA"
         vendor: "NVIDIA Corporation"
         version: "OpenCL 1.2 CUDA 9.1.84"
         profile: "FULL_PROFILE"
         extensions: "cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_fp64 cl_khr_byte_addressable_store cl_khr_icd cl_khr_gl_sharing cl_nv_compiler_options cl_nv_device_attribute_query cl_nv_pragma_unroll cl_nv_copy_opts cl_nv_create_buffer"
  1 device
  1:0 ------------------------------
  Device name: "GeForce GTX 1080"
         global mem cache size: 327680 o = 320 kio
         global mem size: 8511881216 o = 8312384 kio = 8117 Mio = 7 Gio
         local mem size: 49152 o = 48 kio
         max clock frequency: 1733
         max compute units: 20
         max constant buffer size: 65536 o = 64 kio
         max mem alloc size: 2127970304 o = 2078096 kio = 2029 Mio = 1 Gio
         max work group size: 1024 o = 1 kio
         max work item sizes: 1024 1024 64
         opencl c version: "OpenCL C 1.2 "
