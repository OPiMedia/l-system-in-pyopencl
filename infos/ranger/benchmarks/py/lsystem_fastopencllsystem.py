#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
To build lsystem_fastopencllsystem_time.tsv.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 26, 2018
"""

import sys

import parallel
import process as p


########
# Main #
########
def main():
    data = p.load_raw_data(sys.argv[1])

    lsystem = p.filters(data, [(p.K_IMPLEMENTATION, 'LSystem')])
    fastopencllsystem = p.filters(data, [(p.K_IMPLEMENTATION, 'FastOpenCLLSystem'),
                                         (p.K_IS_GPU, True)])
    datas = []
    for k in range(1, 11):
        nb = 2**k
        datas.append((nb, p.filters(fastopencllsystem, [(p.K_NB_WORK_ITEM, nb)])))

    d = {}
    for n in range(1, 21):
        avg = p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.AVG)
        d[(n, )] = ['normalized LSystem',
                    avg,
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.MIN),
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.MAX),
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.CONFIDENCE)]
        for nb, data in datas:
            data_avg = p.data_get_by_key(data, p.K_NB_ITERATION, n, p.AVG)
            d[(n, )].extend(('FastOpenCLLSystem-{}'.format(nb),
                             data_avg,
                             p.data_get_by_key(data, p.K_NB_ITERATION, n, p.MIN),
                             p.data_get_by_key(data, p.K_NB_ITERATION, n, p.MAX),
                             p.data_get_by_key(data, p.K_NB_ITERATION, n, p.CONFIDENCE),
                             parallel.speedup(avg, data_avg) if avg and data_avg else None,
                             parallel.efficiency(avg, data_avg, nb) if avg and data_avg else None,
                             parallel.overhead(avg, data_avg, nb) if avg and data_avg else None))
        d[(n, )] = tuple(d[(n, )])

    p.print_data(d,
                 header=(('# iteration n', '|',
                          'normalized LSystem', 'average', 'min', 'max', 'confidence') +
                         ('FastOpenCLLSystem', 'average', 'min', 'max', 'confidence',
                          'speedup', 'efficiency', 'overhead')*len(datas)))


if __name__ == '__main__':
    main()
