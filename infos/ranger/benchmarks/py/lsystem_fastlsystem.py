#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
To build lsystem_fastlsystem.tsv.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 24, 2018
"""

import sys

import parallel
import process as p
import stats


########
# Main #
########
def main():
    def length(n):
        return 2**(n + 2) - 2

    data = p.load_raw_data(sys.argv[1])

    lsystem = p.filters(data, [(p.K_IMPLEMENTATION, 'LSystem')])
    fastlsystem = p.filters(data, [(p.K_IMPLEMENTATION, 'FastLSystem')])

    avg_avg = stats.geometric_mean([p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.AVG)
                                    for n in range(1, 21)])
    avg_length = stats.geometric_mean([length(n)
                                       for n in range(1, 21)])

    d = {}
    for n in range(1, 21):
        avg = p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.AVG)
        fast_avg = p.data_get_by_key(fastlsystem, p.K_NB_ITERATION, n, p.AVG)
        d[(n, )] = ('LSystem',
                    avg,
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.MIN),
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.MAX),
                    p.data_get_by_key(lsystem, p.K_NB_ITERATION, n, p.CONFIDENCE),

                    'FastLSystem',
                    fast_avg,
                    p.data_get_by_key(fastlsystem, p.K_NB_ITERATION, n, p.MIN),
                    p.data_get_by_key(fastlsystem, p.K_NB_ITERATION, n, p.MAX),
                    p.data_get_by_key(fastlsystem, p.K_NB_ITERATION, n, p.CONFIDENCE),

                    parallel.speedup(avg, fast_avg),
                    parallel.overhead(avg, fast_avg, 1),

                    length(n) * avg_avg / avg_length)

    p.print_data(d,
                 header=('# iteration n', '|',
                         'LSystem', 'average', 'min', 'max', 'confidence',
                         'FastLSystem', 'average', 'min', 'max', 'confidence',
                         'speedup', 'overhead',
                         'l_n x̄/l̄'))


if __name__ == '__main__':
    main()
