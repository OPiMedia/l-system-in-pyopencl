#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
tsv2html

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 26, 2018
"""

from __future__ import division
from __future__ import print_function

import sys


def convert_item(item):
    clas = []

    if isinstance(item, int):
        pass
    else:
        if item.upper() == 'TRUE':
            clas.append('green')
            item = '&#10003;'
        elif item.upper() == 'FALSE':
            clas.append('red')
            item = '&#10005;'

    return (item, clas)


def identify(item):
    if is_bool(item):
        return bool
    elif is_int(item):
        return int
    elif is_float(item):
        return int
    else:
        return None


def is_bool(item):
    return (item.upper() == 'TRUE') or (item.upper() == 'FALSE')


def is_int(item):
    try:
        n = int(item)

        return str(n) == str(item)
    except:
        return False


def is_float(item):
    try:
        n = float(item)

        return str(n) == str(item)
    except:
        return False


def print_footer():
    print("""  </body>
</html>""")


def print_header(title):
    print("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>{}</title>

    <link rel="stylesheet" href="tsv2html/DataTables/datatables.min.css">
    <link rel="stylesheet" href="tsv2html/tsv2html.css">

    <script src="tsv2html/DataTables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="tsv2html/DataTables/datatables.min.js"></script>
    <script>
$(document).ready( function () {{
    $('table.data').DataTable( {{
        "paging": false
    }} );
}} );
    </script>
  </head>
  <body>""".format(title))


def print_data(data, header=None):
    nb_cols, identity, sames, types = process_data(data)

    if header is None:
        header = tuple([None]*nb_cols)
    else:
        header = tuple(header)[:nb_cols]
        header = header + (None, )*(nb_cols - len(header))

    assert len(header) == nb_cols

    print("""    <table class="data display cell-border">
      <thead>
        <tr>""")

    if identity is None:
        print('        <th>i</th>')

    for i, head in enumerate(header):
        if head is None:
            head = i + 1
            title = ''
        else:
            title = ' title="{}"'.format(i)

        if sames[i] is None:
            print('          <th class="sep"{}></th>'.format(title))
        elif sames[i]:
            print('          <th class="same"{}>{}</th>'.format(title, head))
        else:
            print('          <th{}>{}</th>'.format(title, head))

    print("""        </tr>
      </thead>
      <tbody>""")

    for i, items in enumerate(data):
        print('        <tr>')
        if identity is None:
            print('          <th class="int">{}</th>'.format(i + 1))

        for j, item in enumerate(items):
            item, clas = convert_item(item)

            if item == '':
                clas.append('empty')

            if identity == j:
                clas.append('identity')

            if sames[j] is None:
                clas.append('sep')
                item = ''
            elif sames[j]:
                clas.append('same')

            if types[j] == int:
                clas.append('int')

            title_pair = (i + 1, j + 1)

            if clas:
                print('<td class="{}" title="{}">{}</td>'.format(' '.join(clas), title_pair, item))
            else:
                print('<td title="{}">{}</td>'.format(title_pair, item))

        # if column missing then ???

        print('        </tr>')

    print("""      </tbody>
    </table>""")


def process_data(data):
    assert data

    nb_cols = 0
    for items in data:
        nb_cols = max(nb_cols, len(items))

    identity = None

    prevs = list(data[0])
    prevs.extend([None]*(nb_cols - len(prevs)))

    assert len(prevs) == nb_cols

    # ??? also identify row with unique value
    sames = [True]*nb_cols
    types = [identify(item) for item in data[0]]*nb_cols

    for items in data:
        for i, item in enumerate(items):
            if prevs[i] != item:
                sames[i] = False
            prevs[i] = item

    nb_rows = len(data)
    for j in range(nb_cols):
        if identity is None:
            identity = j
            for i in range(nb_rows):
                item = data[i][j]
                if not is_int(item) or (int(item) != i + 1):
                    identity = None

                    break

        if identity is None:
            identity = j
            for i in range(nb_rows):
                item = data[i][j]
                if not is_int(item) or (int(item) != i):
                    identity = None

                    break

        if sames[j] and (data[0][j] == '|'):
            sames[j] = None

    return nb_cols, identity, tuple(sames), tuple(types)


def read_data(filename=None):
    fin = (sys.stdin if filename is None
           else open(filename))

    data = []
    for line in fin:
        if line and (line[-1] == '\n'):
            line = line[:-1]
        pieces = line.split('\t')
        data.append(tuple(pieces))

    if filename is not None:
        fin.close()

    return tuple(data)


########
# Main #
########
def main():
    filename = (sys.argv[1] if len(sys.argv) > 1
                else None)
    title = (sys.argv[2] if len(sys.argv) > 2
             else filename)

    data = read_data(title)

    print_header(filename)
    header, data = data[0], data[1:]
    print_data(data, header)
    print_footer()

if __name__ == '__main__':
    main()
