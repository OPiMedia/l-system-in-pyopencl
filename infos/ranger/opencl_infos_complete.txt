Python version: 2.7.12 (default, Dec  4 2017, 14:50:18)  [GCC 5.4.0 20160609]
PyOpenCL version: 2018.1.1
2 OpenCL platforms
========================================
Platform name: "Intel(R) OpenCL"
         vendor: "Intel(R) Corporation"
         version: "OpenCL 2.0 "
         profile: "FULL_PROFILE"
         extensions: "cl_khr_icd cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_byte_addressable_store cl_khr_depth_images cl_khr_3d_image_writes cl_intel_exec_by_local_thread cl_khr_spir cl_khr_fp64 cl_khr_image2d_from_buffer"
  1 device
  0:0 ------------------------------
  Device name: "Intel(R) Core(TM) i7-6900K CPU @ 3.20GHz"
         address bits: 64
         available: 1
         compiler available: 1
         double fp config: 63
         driver version: "1.2.0.475"
         endian little: 1
         error correction support: 0
         execution capabilities: 3
         extensions: "cl_khr_icd cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_byte_addressable_store cl_khr_depth_images cl_khr_3d_image_writes cl_intel_exec_by_local_thread cl_khr_spir cl_khr_fp64 cl_khr_image2d_from_buffer "
         global mem cache size: 262144 o = 256 kio
         global mem cache type: 2
         global mem cacheline size: 64 o
         global mem size: 101281853440 o = 98908060 kio = 96589 Mio = 94 Gio
         global variable preferred total size: 65536 o = 64 kio
         host unified memory: 1
         image2d max height: 16384
         image2d max width: 16384
         image3d max depth: 2048
         image3d max height: 2048
         image3d max width: 2048
         image max array size: 2048 o = 2 kio
         image max buffer size: 1582528960 o = 1545438 kio = 1509 Mio = 1 Gio
         image support: 1
         int ptr: 21214872
         linker available: 1
         local mem size: 32768 o = 32 kio
         local mem type: 2
         max clock frequency: 3200
         max compute units: 16
         max constant args: 480
         max constant buffer size: 131072 o = 128 kio
         max global variable size: 65536 o = 64 kio
         max mem alloc size: 25320463360 o = 24727015 kio = 24147 Mio = 23 Gio
         max on device events: 4294967295
         max on device queues: 4294967295
         max parameter size: 3840 o = 3 kio
         max pipe args: 16
         max read image args: 480
         max read write image args: 480
         max samplers: 480
         max work group size: 8192 o = 8 kio
         max work item dimensions: 3
         max work item sizes: 8192 8192 8192
         max write image args: 480
         mem base addr align: 1024
         min data type align size: 128 o
         native vector width char: 32
         native vector width double: 4
         native vector width float: 8
         native vector width half: 0
         native vector width int: 8
         native vector width long: 4
         native vector width short: 16
         opencl c version: "OpenCL C 2.0 "
         partition affinity domain: 0
         partition max sub devices: 16
         partition properties: 4231 4230 16466
         partition type: 
         persistent unique id: Intel(R) Corporation 32902 Intel(R) Core(TM) i7-6900K CPU @ 3.20GHz OpenCL 2.0 (Build 475)
         pipe max active reservations: 16383
         pipe max packet size: 1024 o = 1 kio
         preferred global atomic alignment: 64
         preferred interop user sync: 0
         preferred local atomic alignment: 0
         preferred platform atomic alignment: 64
         preferred vector width char: 1
         preferred vector width double: 1
         preferred vector width float: 1
         preferred vector width half: 0
         preferred vector width int: 1
         preferred vector width long: 1
         preferred vector width short: 1
         profile: "FULL_PROFILE"
         profiling timer resolution: 1
         queue on device max size: 4294967295 o = 4194303 kio = 4095 Mio = 3 Gio
         queue on device preferred size: 4294967295 o = 4194303 kio = 4095 Mio = 3 Gio
         queue on device properties: 3
         queue on host properties: 3
         queue properties: 3
         reference count: 1
         single fp config: 7
         spir versions: "1.2"
         svm capabilities: 15
         type: 2
         vendor: "Intel(R) Corporation"
         vendor id: 32902
         version: "OpenCL 2.0 (Build 475)"
========================================
Platform name: "NVIDIA CUDA"
         vendor: "NVIDIA Corporation"
         version: "OpenCL 1.2 CUDA 9.1.84"
         profile: "FULL_PROFILE"
         extensions: "cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_fp64 cl_khr_byte_addressable_store cl_khr_icd cl_khr_gl_sharing cl_nv_compiler_options cl_nv_device_attribute_query cl_nv_pragma_unroll cl_nv_copy_opts cl_nv_create_buffer"
  1 device
  1:0 ------------------------------
  Device name: "GeForce GTX 1080"
         address bits: 64
         attribute async engine count nv: 2
         available: 1
         built in kernels: ""
         compiler available: 1
         compute capability major nv: 6
         compute capability minor nv: 1
         double fp config: 63
         driver version: "390.67"
         endian little: 1
         error correction support: 0
         execution capabilities: 1
         extensions: "cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_fp64 cl_khr_byte_addressable_store cl_khr_icd cl_khr_gl_sharing cl_nv_compiler_options cl_nv_device_attribute_query cl_nv_pragma_unroll cl_nv_copy_opts cl_nv_create_buffer"
         global mem cache size: 327680 o = 320 kio
         global mem cache type: 2
         global mem cacheline size: 128 o
         global mem size: 8511881216 o = 8312384 kio = 8117 Mio = 7 Gio
         gpu overlap nv: 1
         host unified memory: 0
         image2d max height: 32768
         image2d max width: 16384
         image3d max depth: 16384
         image3d max height: 16384
         image3d max width: 16384
         image max array size: 2048 o = 2 kio
         image max buffer size: 134217728 o = 131072 kio = 128 Mio
         image support: 1
         int ptr: 21080848
         integrated memory nv: 0
         kernel exec timeout nv: 0
         linker available: 1
         local mem size: 49152 o = 48 kio
         local mem type: 1
         max clock frequency: 1733
         max compute units: 20
         max constant args: 9
         max constant buffer size: 65536 o = 64 kio
         max mem alloc size: 2127970304 o = 2078096 kio = 2029 Mio = 1 Gio
         max on device events: 2048
         max on device queues: 4
         max parameter size: 4352 o = 4 kio
         max read image args: 256
         max samplers: 32
         max work group size: 1024 o = 1 kio
         max work item dimensions: 3
         max work item sizes: 1024 1024 64
         max write image args: 16
         mem base addr align: 4096
         min data type align size: 128 o
         native vector width char: 1
         native vector width double: 1
         native vector width float: 1
         native vector width half: 0
         native vector width int: 1
         native vector width long: 1
         native vector width short: 1
         opencl c version: "OpenCL C 1.2 "
         partition affinity domain: 0
         partition max sub devices: 1
         partition properties: 0
         partition type: 0
         pci bus id nv: 6
         pci slot id nv: 0
         persistent unique id: NVIDIA Corporation 4318 GeForce GTX 1080 OpenCL 1.2 CUDA
         preferred interop user sync: 0
         preferred vector width char: 1
         preferred vector width double: 1
         preferred vector width float: 1
         preferred vector width half: 0
         preferred vector width int: 1
         preferred vector width long: 1
         preferred vector width short: 1
         profile: "FULL_PROFILE"
         profiling timer resolution: 1000
         queue on device max size: 262144 o = 256 kio
         queue on device preferred size: 262144 o = 256 kio
         queue on device properties: 3
         queue on host properties: 3
         queue properties: 3
         reference count: 1
         registers per block nv: 65536
         single fp config: 191
         svm capabilities: 1
         type: 4
         vendor: "NVIDIA Corporation"
         vendor id: 4318
         version: "OpenCL 1.2 CUDA"
         warp size nv: 32
