import l_system.simple_l_system

production = {'A': 'AB',
              'B': 'A'}
algae = l_system.simple_l_system.LSystem('AB', 'A', production)
algae.expand(2)
print(algae)  # print the 2th iterated: ABA
algae.expand(3)
print(algae)  # print the 5th iterated: ABAABABAABAAB

iter_rule = algae.production_iterate(3)
print(iter_rule)  # print the 3th iterated of the rule themselves:
                  # {'A': 'ABAAB',
                  #  'B': 'ABA'}
